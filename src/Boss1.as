package  {
	import adobe.utils.ProductManager;
	import org.flixel.*;
	import org.flixel.plugin.photonstorm.*;
	
	public class Boss1 extends Enemy {
		[Embed(source='../img/boss_1.png')]
		private var ImgShip:Class;
		[Embed(source='../img/enemyshot_1.png')]
		private var ImgBullet:Class;
		[Embed(source='../img/enemyshot_4.png')]
		private var ImgBullet2:Class;
		[Embed(source='../img/enemyshot_6.png')]
		private var ImgBullet3:Class;
		[Embed(source='../sfx/enemyshot_1.mp3')]
		private var SfxShot:Class;
		
		public var shotTimer:int = 0;
		public var shotTimer2:int = 0;
		public var angle2:Number = 0;
		public var angle3:Number = 0;
		
		public function Boss1(X:Number=0, Y:Number=0) {
			super(X, Y, ImgShip);
		}
		
		override public function maxHealth():Number {
			return 1200;
		}
		
		override public function damage():Number {
			return 30;
		}
		
		override public function update():void {
			super.update();
			
			angle += 1;
			
			var dist:FlxPoint = new FlxPoint((PlayState.State().playerShip.x + PlayState.State().playerShip.width / 2) - (x + width / 2),
											 (PlayState.State().playerShip.y + PlayState.State().playerShip.height / 2) - (y + height / 2));
			var distance:Number = Math.sqrt(dist.x * dist.x + dist.y * dist.y);

				if (distance > 300) {
					x += dist.x / distance;
					y += dist.y / distance;
					FlxG.collide(this, PlayState.State().currentLevel.tilemaps);
				}
			// Shoot.
			if (shotTimer == 0) {
					var angle:Number = FlxVelocity.angleBetween(this, PlayState.State().playerShip, false);
					
					if (FlxG.random() < 0.5) {
				for (var i:int = 0; i <= 10; ++i) {
					var bullet:EnemyBullet =
						PlayState.State().bullets.recycle(EnemyBullet) as EnemyBullet;
					bullet.loadGraphic(ImgBullet2);
					bullet.reset(x + width / 2 - bullet.width / 2, y + height / 2 - bullet.height / 2);
					bullet.damage = 15;
					
					bullet.velocity.x = 4 * (Math.cos(angle + 360 / 10 * i));
					bullet.velocity.y = 4 * (Math.sin(angle + 360 / 10 * i));
					
					//bullet.velocity = FlxVelocity.velocityFromAngle(angle + 360 / 8 * i, 4);
				}
					} else {
				
				for (i = 0; i <= 3; ++i) {
					bullet =
						PlayState.State().bullets.recycle(EnemyBullet) as EnemyBullet;
					bullet.loadGraphic(ImgBullet);
					bullet.reset(x + width / 2 - bullet.width / 2, y + height / 2 - bullet.height / 2);
					bullet.damage = 15;

					bullet.velocity.x = (3 + i * 1.5) * (Math.cos(angle));
					bullet.velocity.y = (3 + i * 1.5) * (Math.sin(angle));
					
					//bullet.velocity = FlxVelocity.velocityFromAngle(angle, 3 + i * 1.5);
				}
					}
				shotTimer = 40;
				FlxG.play(SfxShot);
			} else {
				shotTimer--;
			}
			
			if (shotTimer2 == 0) {
					bullet =
						PlayState.State().bullets.recycle(EnemyBullet) as EnemyBullet;
					bullet.loadGraphic(ImgBullet2);
					bullet.reset(x + width / 2 - bullet.width / 2, y + height / 2 - bullet.height / 2);
					bullet.damage = 15;
					//bullet.velocity = FlxVelocity.velocityFromAngle(angle2, 3);
					bullet.velocity.x = 3 * (Math.cos(angle2));
					bullet.velocity.y = 3 * (Math.sin(angle2));
					
					if (Difficulty.difficulty != Difficulty.EASY) {
					bullet =
						PlayState.State().bullets.recycle(EnemyBullet) as EnemyBullet;
					bullet.loadGraphic(ImgBullet3);
					bullet.reset(x + width / 2 - bullet.width / 2, y + height / 2 - bullet.height / 2);
					bullet.damage = 15;
//					bullet.velocity = FlxVelocity.velocityFromAngle(angle3, 3);
					bullet.velocity.x = 3 * (Math.cos(angle3));
					bullet.velocity.y = 3 * (Math.sin(angle3));
					}
					angle2 += 33.57;
					angle3 += 26 * 4;
				
				shotTimer2 = 6;
				FlxG.play(SfxShot);
			} else {
				shotTimer2--;
			}
		}
		
		override public function hurt(Damage:Number):void {
			super.hurt(Damage);
			
			if (health <= 0) {
				// Flash.
				FlxG.flash(0xffffffff, 1);
				
				// Shake.
				FlxG.shake();
				
				// END GAME HERE
				FlxG.music.fadeOut(3);
				FlxG.fade(0xff000000, 3, endgame);
			}
		}
		
		override public function kill():void {
			super.kill();
		}
		
		public function endgame():void {
			PlayState.State().playerShip.SoundCharged.volume = 0;
			FlxSpecialFX.clear();
			FlxG.switchState(new EndState());
		}
	}
}