package {
	import org.flixel.*;
	import org.flixel.plugin.photonstorm.*;
	
	public class PlayerBullet extends FlxSprite {
		[Embed(source='../img/playerbullet_1.png')]
		private var ImgBasic:Class;
		[Embed(source='../img/playerbullet_2.png')]
		private var ImgCharged:Class;
		[Embed(source='../img/playerbullet_3.png')]
		private var ImgGreen:Class;
		[Embed(source='../img/playerbullet_4.png')]
		private var ImgGreenCharged:Class;
		
		public var charged:Boolean;
		
		public var lifetime:int;
		
		override public function reset(X:Number, Y:Number):void {
			super.reset(X, Y);
			
			// live for one second
			lifetime = 27;

			// Needed for weird flip case.
			frame = 0;
			
			charged = PlayState.State().playerShip.charged();
			switch (PlayState.State().playerShip.weaponType) {
				case PlayerShip.WEAPON_BASIC:
					loadGraphic(ImgBasic, false, true);
					break;
				case PlayerShip.WEAPON_CHARGE:
					if (charged) {
						loadGraphic(ImgCharged, false, true);
					} else {
						loadGraphic(ImgBasic, false, true);
					}
					break;
				case PlayerShip.WEAPON_GREEN:
					if (charged) {
						loadGraphic(ImgGreenCharged, false, true);
					} else {
						loadGraphic(ImgGreen, false, true);
					}
					break;
				default:
					break;
			}

			y += PlayState.State().playerShip.height * 3 / 4;
			y -= height * 2 / 3;
			facing = PlayState.State().playerShip.facing;
			if (facing == LEFT) {
				x -= width - width/2;
			} else {
				x += PlayState.State().playerShip.width - width/2;
			}
		}
		
		override public function update():void {
			super.update();
			
			lifetime--;
			if (lifetime == 0) {
				exists = false;
				return;
			}
			
			var speed:Number;
			var damage:Number;
			switch (PlayState.State().playerShip.weaponType) {
				case PlayerShip.WEAPON_BASIC:
					speed = 20;
					damage = 2;
					break;
				case PlayerShip.WEAPON_CHARGE:
					if (charged) {
						speed = 20;
						damage = 16;
					} else {
						speed = 20;
						damage = 2;
					}
					break;
				case PlayerShip.WEAPON_GREEN:
					if (charged) {
						speed = 20;
						damage = 48;
					} else {
						speed = 20;
						damage = 6;
					}
					break;
				default:
					break;
			}
			if (facing == LEFT) {
				x -= speed;
			} else {
				x += speed;
			}
			
			// Collide with enemies.
			for each (var group:Object in PlayState.State().currentLevel.masterLayer.members) {
				if (group is FlxGroup) {
					for each (var enemy:Object in group.members) {
						if (enemy is Enemy) {
							if (FlxCollision.pixelPerfectCheck(this, enemy as Enemy)) {
								if ((enemy as Enemy).exists) {
									(enemy as Enemy).hurt(damage);
									exists = false;
									return;
								}
							}
						}
					}
				}
			}
			
			// Collide with blocks.
			for each (var group2:Object in PlayState.State().currentLevel.masterLayer.members) {
				if (group2 is FlxGroup) {
					for each (var enemy2:Object in group2.members) {
						if (enemy2 is Block) {
							if (FlxCollision.pixelPerfectCheck(this, enemy2 as Block)) {
								if ((enemy2 as Block).exists) {
			if (PlayState.State().playerShip.weaponType >= PlayerShip.WEAPON_GREEN) {
									(enemy2 as Block).hurt(10);
			}
			if (!charged) {
									exists = false;
									return;
			}
								}
							}
						}
					}
				}
			}
			// Collide with charge blocks.
			for each (var group3:Object in PlayState.State().currentLevel.masterLayer.members) {
				if (group3 is FlxGroup) {
					for each (var enemy3:Object in group3.members) {
						if (enemy3 is BlockCharged) {
							if (FlxCollision.pixelPerfectCheck(this, enemy3 as BlockCharged)) {
								if ((enemy3 as BlockCharged).exists) {
			if (charged) {
									(enemy3 as BlockCharged).hurt(10);
			} else {
									exists = false;
									return;
			}
								}
							}
						}
					}
				}
			}
			
			// Collide with terrain.
			if (FlxG.collide(this, PlayState.State().currentLevel.tilemaps)) {
				exists = false;
			}
		}
	}
}