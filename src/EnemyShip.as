package  {
	import org.flixel.*;
	import org.flixel.plugin.photonstorm.*;
	
	public class EnemyShip extends Enemy {
		[Embed(source='../img/enemyship_1.png')]
		private var ImgShip:Class;
		
		public function EnemyShip(X:Number=0, Y:Number=0) {
			super(X, Y, ImgShip);
		}
		
		override public function maxHealth():Number {
			return 15;
		}
		
		override public function damage():Number {
			return 20;
		}
		
		override public function update():void {
			super.update();
			
			// Try to move towards player if within radius.
			
			var dist:FlxPoint = new FlxPoint((PlayState.State().playerShip.x + PlayState.State().playerShip.width / 2) - (x + width / 2),
											 (PlayState.State().playerShip.y + PlayState.State().playerShip.height / 2) - (y + height / 2));
			var distance:Number = Math.sqrt(dist.x * dist.x + dist.y * dist.y);
			
			if (distance < 400) {
				//move towards
				dist.x /= distance;
				dist.y /= distance;
				dist.x *= 2;
				dist.y *= 2;
				x += dist.x;
				y += dist.y;
			
			// Collide with enemies.
			for each (var group:Object in PlayState.State().currentLevel.masterLayer.members) {
				if (group is FlxGroup) {
					for each (var enemy:Object in group.members) {
						if (enemy is Enemy) {
							FlxG.collide(this, enemy as Enemy);
						}
					}
				}
			}			

			}
			
			FlxG.collide(this, PlayState.State().currentLevel.tilemaps);
		}
	}
}