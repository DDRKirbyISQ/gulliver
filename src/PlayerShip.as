package {
	import com.eclecticdesignstudio.motion.easing.equations.ExpoEaseIn;
	import org.flixel.*;
	import org.flixel.plugin.photonstorm.*;
	import flash.geom.ColorTransform;
	
	public class PlayerShip extends FlxSprite {
		[Embed(source='../img/playership.png')]
		private var ImgShip:Class;
		[Embed(source='../sfx/shotbasic.mp3')]
		private var SfxBasic:Class;
		[Embed(source='../sfx/shotcharge.mp3')]
		private var SfxCharge:Class;
		[Embed(source='../sfx/shrink.mp3')]
		private var SfxShrink:Class;
		[Embed(source='../sfx/error.mp3')]
		private var SfxError:Class;
		[Embed(source='../sfx/hurt.mp3')]
		private var SfxHurt:Class;
		[Embed(source='../sfx/charging.mp3')]
		private var SfxCharging:Class;
		[Embed(source='../sfx/charged.mp3')]
		private var SfxCharged:Class;
		[Embed(source='../sfx/chargeready.mp3')]
		private var SfxChargeReady:Class;
		[Embed(source = '../sfx/shotgreen.mp3')]
		private var SfxGreen:Class;
		[Embed(source='../sfx/shotgreencharged.mp3')]
		private var SfxGreenCharged:Class;
		[Embed(source='../sfx/playerdie.mp3')]
		private var SfxDie:Class;

		public var SoundCharging:FlxSound;
		public var SoundCharged:FlxSound;
		
		private static const kShrinkSpeed:Number = 0.05;
		private static const kShrinkMoveSpeed:Number = 2;
		private static const kShrinkFactor:Number = 0.25;
		private static const kInvincibleTime:int = 60;
		private static const kMaxCharge:int = 45;

		private static const kChargeCycleTime:int = 30;
		
		public var shrunk:Boolean = false;
		public var moveSpeed:Number = 6.0 * 1;
		public var weaponType:uint = WEAPON_BASIC;
		public var cooldownTimer:int = 0;
		public var invincibilityTimer:int = 0;
		public var charge:int = 0;
		public var chargeCycler:int = 0;
		
		public static const WEAPON_BASIC:uint = 0;
		public static const WEAPON_CHARGE:uint = 1;
		public static const WEAPON_GREEN:uint = 2;
		
		public function PlayerShip(X:Number = 0, Y:Number = 0) {
			super(X, Y);
			loadGraphic(ImgShip, false, true);
			antialiasing = true;
			health = Difficulty.maxHealth();
			
			SoundCharging = new FlxSound();
			SoundCharging.loadEmbedded(SfxCharging, false, false);
			SoundCharged = new FlxSound();
			SoundCharged.loadEmbedded(SfxCharged, true, false);
			SoundCharged.volume = 0;
			SoundCharged.play(false);
		}
		
		public function respawn(X:Number, Y:Number):void {
			x = X;
			y = Y;
			x -= width / 2;
			y -= height / 2;
			health = Difficulty.maxHealth();
			exists = true;
		}
		
		override public function update():void {
			moves = false;
			super.update();
			movement();

			// Shrinking.
			handleShrink();
			
			if (charge == kMaxCharge - 1) {
				FlxG.play(SfxChargeReady);
			}
			
			// Charge flash.
			var colorChange:Number = charge * 255 / kMaxCharge;
			var mult:Number = chargeCycler / kChargeCycleTime;
			if (charged()) mult = chargeCycler / (kChargeCycleTime / 4);
			if (mult > 0.5) mult = 1.0 - mult;
			mult *= 2;
			colorChange *= mult;
			_colorTransform = new ColorTransform(1, 1, 1, 1, colorChange / 4, colorChange / 4, -colorChange, 0);
			calcFrame();
			
			chargeCycler++;
			if (chargeCycler > kChargeCycleTime) {
				chargeCycler = 0;
			}
			if (charged() && chargeCycler > kChargeCycleTime / 4) {
				chargeCycler = 0;
			}
			
			// Collision.
			FlxG.collide(this, PlayState.State().currentLevel.tilemaps);
			
			// Collide with enemies.
			if (invincibilityTimer == 0) {
				for each (var group:Object in PlayState.State().currentLevel.masterLayer.members) {
					if (group is FlxGroup) {
						for each (var enemy:Object in group.members) {
							if (enemy is Enemy) {
								var collid:Boolean;
								if (shrunk) {
									collid = FlxG.overlap(this, enemy as Enemy);
								} else {
									collid = FlxCollision.pixelPerfectCheck(this, enemy as Enemy);
								}
								if (collid) {
									if ((enemy as Enemy).exists) {
										hurt((enemy as Enemy).damage());
										if (!exists) {
											return;
										}
									}
								}
							}
						}
					}
				}			
			} else {
				invincibilityTimer--;
			}

			// Shooting.
			if (!shrunk) {
				if (cooldownTimer > 0) {
					--cooldownTimer;
				} else if (weaponType >= WEAPON_CHARGE) {
					// charge
					if (FlxG.keys.Z) {
						charge++;
						if (charge == 5) {
							SoundCharging.play(true);
						}
						if (charge > kMaxCharge) {
							SoundCharged.volume = 1.0;
							charge = kMaxCharge;
						}
					}
					
					if (FlxG.keys.justReleased("Z")) {
						fireShot();

						charge = 0;
						SoundCharged.volume = 0;
						SoundCharging.stop();
					}
				} else if (FlxG.keys.Z) {
					fireShot();
				}
			}
		}
		
		public function shrink():void {
			shrunk = !shrunk;
			if (shrunk) {
				width = width * kShrinkFactor;
				height = height * kShrinkFactor;
				centerOffsets(true);
			} else {
				width = width / kShrinkFactor;
				height = height / kShrinkFactor;
				x -= offset.x;
				y -= offset.y;
				centerOffsets(false);
			}
		}

		public function shrinkInstant():void {
			shrink();
			if (shrunk) {
				scale.x = kShrinkFactor;
				scale.y = kShrinkFactor;
			} else {
				scale.x = 1;
				scale.y = 1;
			}
		}
		
		protected function movement():void {
			if (FlxG.keys.LEFT) {
				if (shrunk) x -= moveSpeed / kShrinkMoveSpeed;
				else
				x -= moveSpeed;
				facing = LEFT;
				FlxG.collide(this, PlayState.State().currentLevel.tilemaps);			
			}
			if (FlxG.keys.RIGHT) {
				if (shrunk) x += moveSpeed / kShrinkMoveSpeed;
				else
				x += moveSpeed;
				facing = RIGHT;
				FlxG.collide(this, PlayState.State().currentLevel.tilemaps);			
			}
			if (FlxG.keys.DOWN) {
				if (shrunk) y += moveSpeed / kShrinkMoveSpeed;
				else
				y += moveSpeed;
				FlxG.collide(this, PlayState.State().currentLevel.tilemaps);			
			}
			if (FlxG.keys.UP) {
				if (shrunk) y -= moveSpeed / kShrinkMoveSpeed;
				else
				y -= moveSpeed;
				FlxG.collide(this, PlayState.State().currentLevel.tilemaps);			
			}
		}
		
		public function charged():Boolean {
			return charge == kMaxCharge;
		}
		
		private function fireShot():void {
			var bullet:PlayerBullet =
				PlayState.State().bullets.recycle(PlayerBullet)
				as PlayerBullet;
			bullet.reset(x, y);
			switch (weaponType) {
				case WEAPON_BASIC:
					cooldownTimer = 10;
					FlxG.play(SfxBasic);
					break;
				case WEAPON_CHARGE:
					cooldownTimer = 6;
					if (charged()) {
						FlxG.play(SfxCharge);
					} else {
						FlxG.play(SfxBasic);
					}
					break;
				case WEAPON_GREEN:
					cooldownTimer = 6;
					if (charged()) {
						FlxG.play(SfxGreenCharged);
					} else {
						FlxG.play(SfxGreen);
					}
					break;
				default:
					break;
			}
		}
		
		private function handleShrink():void {
			if (FlxG.keys.justPressed("X")) {
				if (shrunk && scale.x != kShrinkFactor) {
					// don't do anything
				} else if (!shrunk && scale.x != 1) {
					// don't do anything
				} else {
					if (shrunk) {
						// Check for whether there will be a collision after unshrinking.
						var originalX:Number = x;
						var originalY:Number = y;
						shrink();

						// Padding.
						width += 60;
						height += 60;
						x -= 30;
						y -= 30;

						var oldX:Number = x;
						var oldY:Number = y;
						var collid:Boolean = FlxG.collide(this, PlayState.State().currentLevel.tilemaps);
			// Collide with blocks
			for each (var group:Object in PlayState.State().currentLevel.masterLayer.members) {
				if (group is FlxGroup) {
					for each (var block:Object in group.members) {
						if (block is Block) {
							if (FlxG.collide(this, block as Block)) {
								if ((block as Block).exists) {
									collid = true;
								}
							}
							if ((block as Block).exists) {
								if (Math.abs(x + width/2 - (block as Block).x + 16) < 50 &&
								Math.abs(y + height/2 - (block as Block).y + 16) < 50) {
									collid = true;
								}
							}
						}
					}
				}
			}
			// Collide with blocks
			for each (var group2:Object in PlayState.State().currentLevel.masterLayer.members) {
				if (group2 is FlxGroup) {
					for each (var block2:Object in group2.members) {
						if (block2 is BlockCharged) {
							if (FlxG.collide(this, block2 as BlockCharged)) {
								if ((block2 as BlockCharged).exists) {
									collid = true;
								}
							}
							if ((block2 as BlockCharged).exists) {
								if (Math.abs(x + width/2 - (block2 as BlockCharged).x + 16) < 50 &&
								Math.abs(y + height/2 - (block2 as BlockCharged).y + 16) < 50) {
									collid = true;
								}
							}
						}
					}
				}
			}			
			if (collid || oldX != x || oldY != y) {
							// Error, couldn't shrink
							FlxG.play(SfxError);
							PlayState.State().errorText.alpha = 1;
							width -= 60;
							height -= 60;
							shrink();
							x = originalX;
							y = originalY;
						} else {
							width -= 60;
							height -= 60;
							x += 30;
							y += 30;
							FlxG.play(SfxShrink);
						}
					} else {
						// You can always shrink.
						shrink();
						FlxG.play(SfxShrink);
						charge = 0;
						SoundCharged.volume = 0;
					}
				}
			}
			
			if (shrunk) {
				scale.x -= kShrinkSpeed;
				if (scale.x < kShrinkFactor) {
					scale.x = kShrinkFactor;
				}
				scale.y -= kShrinkSpeed;
				if (scale.y < kShrinkFactor) {
					scale.y = kShrinkFactor;
				}
			} else {
				scale.x += kShrinkSpeed;
				if (scale.x > 1) {
					scale.x = 1;
				}
				scale.y += kShrinkSpeed;
				if (scale.y > 1) {
					scale.y = 1;
				}
			}
		}
		
		override public function hurt(Damage:Number):void {
			super.hurt(Damage);
			
			FlxG.play(SfxHurt);
			invincibilityTimer = kInvincibleTime;
			flicker(kInvincibleTime / 60);
		}
		
		override public function kill():void {
			super.kill();
			
			PlayState.State().explosions.Explode(x + width / 2, y + height / 2, 100);
			FlxG.play(SfxDie);
			
			PlayState.State().wedied();
		}
	}
}