package {
	import org.flixel.*;
	
	public class Spawner extends FlxSprite {
		[Embed(source='../img/spawner.png')]
		private var ImgSpawner:Class;
		[Embed(source='../sfx/spawner.mp3')]
		private var SfxSpawner:Class;
		
		public function Spawner(X:Number = 0, Y:Number = 0) {
			super(X, Y);
			loadGraphic(ImgSpawner, true, false, 64, 64);
			addAnimation("inactive", [0], 30);
			addAnimation("active", [1, 2, 3, 4], 30);
			play("active");
		}
		
		override public function update():void {
			super.update();
			
			if (PlayState.State().respawnX == x + 32 && PlayState.State().respawnY == y + 32) {
				// we're active.
				play("active");
			} else {
				play("inactive");
				
				if (FlxG.overlap(this, PlayState.State().playerShip)) {
					PlayState.State().respawnX = x + 32;
					PlayState.State().respawnY = y + 32;
					FlxG.play(SfxSpawner);
					PlayState.State().saveText.alpha = 1;
				}
			}
		}
	}
}