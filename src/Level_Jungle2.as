//Code generated with DAME. http://www.dambots.com

package 
{
	import org.flixel.*;
	import flash.utils.Dictionary;
	public class Level_Jungle2 extends BaseLevel
	{
		//Embedded media...
		[Embed(source="../map/mapCSV_Jungle2_Map2.csv", mimeType="application/octet-stream")] public var CSV_Map2:Class;
		[Embed(source="../map/tiles_jungle.png")] public var Img_Map2:Class;

		//Tilemaps
		public var layerMap2:FlxTilemap;

		//Sprites
		public var Layer1Group:FlxGroup = new FlxGroup;

		//Properties


		public function Level_Jungle2(addToStage:Boolean = true, onAddCallback:Function = null, parentObject:Object = null)
		{
			// Generate maps.
			var properties:Array = [];
			var tileProperties:Dictionary = new Dictionary;

			properties = generateProperties( null );
			layerMap2 = addTilemap( CSV_Map2, Img_Map2, 0.000, 0.000, 32, 32, 1.000, 1.000, false, 1, 1, properties, onAddCallback );

			//Add layers to the master group in correct order.
			masterLayer.add(Layer1Group);
			masterLayer.add(layerMap2);

			if ( addToStage )
				createObjects(onAddCallback, parentObject);

			boundsMinX = 0;
			boundsMinY = 0;
			boundsMaxX = 3200;
			boundsMaxY = 1600;
			bgColor = 0xff777777;
		}

		override public function createObjects(onAddCallback:Function = null, parentObject:Object = null):void
		{
			addSpritesForLayerLayer1(onAddCallback);
			generateObjectLinks(onAddCallback);
			if ( parentObject != null )
				parentObject.add(masterLayer);
			else
				FlxG.state.add(masterLayer);
		}

		public function addSpritesForLayerLayer1(onAddCallback:Function = null):void
		{
			addSpriteToLayer(new BigPortal(1344.000, 928.000, 862, 1107, "space2"), BigPortal, Layer1Group , 1344.000, 928.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"jungle2tospace2"
			addSpriteToLayer(new Portal(672.000, 1120.000, 3744, 978, "cave2"), Portal, Layer1Group , 672.000, 1120.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"jungle2tocave2"
		}

		public function generateObjectLinks(onAddCallback:Function = null):void
		{
		}

	}
}
