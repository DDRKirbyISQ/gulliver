package  {
	import org.flixel.*;
	
	public class BlockCharged extends FlxSprite {
		[Embed(source='../img/blockcharged.png')]
		private var ImgBlock:Class;
		[Embed(source='../sfx/blockdie.mp3')]
		private var SfxDie:Class;
		
		public function BlockCharged(X:Number=0, Y:Number=0) {
			super(X, Y, ImgBlock);
			
			immovable = true;
			health = 1;
		}
		
		override public function update():void {
			super.update();
			
			FlxG.collide(PlayState.State().playerShip, this);
		}
		
		override public function hurt(Damage:Number):void {
			super.hurt(Damage);

			if (health <= 0) {
				PlayState.State().explosions.Explode(x + width / 2, y + height / 2, 10);
				FlxG.play(SfxDie);
			}
		}
		
		override public function kill():void {
			super.kill();
		}
	}
}