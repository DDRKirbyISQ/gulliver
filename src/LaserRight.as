package {
	import org.flixel.*;
	
	public class LaserRight extends FlxSprite {
		[Embed(source='../img/laservert.png')]
		private var ImgLaser:Class;
		[Embed(source='../sfx/laser.mp3')]
		private var SfxLaser:Class;
		
		public var ready:Boolean;
		
		public function LaserRight(X:Number = 0, Y:Number = 0, Active:Boolean = false) {
			super(X, Y, ImgLaser);
			
			immovable = true;
			
			if (Active) {
				alpha = 1;
			} else {
				// inactive
				alpha = 0;
			}
			
			ready = false;
		}
		
		override public function update():void {
			super.update();
			
			if (alpha > 0) {
				FlxG.collide(PlayState.State().playerShip, this);
			} else {
				//triggering logic
				if (FlxG.overlap(PlayState.State().playerShip, this)) {
					ready = true;
				}
				
				if (ready) {
					
					var playerX:Number = PlayState.State().playerShip.x + PlayState.State().playerShip.width / 2;
					var thisX:Number = x + width / 2;
					// if you move to the left, turn unready
					if (thisX - playerX > 100) {
						ready = false;
					}
					
					// if you move to the right, turn on
					if (playerX - thisX > 100) {
						alpha = 1;
						FlxG.flash(0x99ffaaaa, 0.5);
						FlxG.play(SfxLaser);
					}
				}
			}
		}
	}
}