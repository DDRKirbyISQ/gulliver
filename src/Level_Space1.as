//Code generated with DAME. http://www.dambots.com

package 
{
	import org.flixel.*;
	import flash.utils.Dictionary;
	public class Level_Space1 extends BaseLevel
	{
		//Embedded media...
		[Embed(source="../map/mapCSV_Space1_Map2.csv", mimeType="application/octet-stream")] public var CSV_Map2:Class;
		[Embed(source="../map/tiles_space.png")] public var Img_Map2:Class;

		//Tilemaps
		public var layerMap2:FlxTilemap;

		//Sprites
		public var Layer1Group:FlxGroup = new FlxGroup;

		//Properties


		public function Level_Space1(addToStage:Boolean = true, onAddCallback:Function = null, parentObject:Object = null)
		{
			// Generate maps.
			var properties:Array = [];
			var tileProperties:Dictionary = new Dictionary;

			properties = generateProperties( null );
			layerMap2 = addTilemap( CSV_Map2, Img_Map2, 0.000, 0.000, 32, 32, 1.000, 1.000, false, 1, 1, properties, onAddCallback );

			//Add layers to the master group in correct order.
			masterLayer.add(Layer1Group);
			masterLayer.add(layerMap2);

			if ( addToStage )
				createObjects(onAddCallback, parentObject);

			boundsMinX = 0;
			boundsMinY = 0;
			boundsMaxX = 8000;
			boundsMaxY = 6400;
			bgColor = 0xff777777;
		}

		override public function createObjects(onAddCallback:Function = null, parentObject:Object = null):void
		{
			addSpritesForLayerLayer1(onAddCallback);
			generateObjectLinks(onAddCallback);
			if ( parentObject != null )
				parentObject.add(masterLayer);
			else
				FlxG.state.add(masterLayer);
		}

		public function addSpritesForLayerLayer1(onAddCallback:Function = null):void
		{
			addSpriteToLayer(new LaserRight(736.000, 672.000, true), LaserRight, Layer1Group , 736.000, 672.000, 0.000, 1, 1, false, 1.000, 11.000, generateProperties( null ), onAddCallback );//"laserrightactive"
			addSpriteToLayer(new DialogTrigger(768.000, 512.000, "U.S.S. Gulliver reporting in.\n\nI've successfully infiltrated\nthe station.\n\nSomething tells me this isn't\ngoing to be easy, but we'll\nfind out soon enough...\n\n<Controls: Arrow keys to move>\n\n-Press SPACE to continue-"), DialogTrigger, Layer1Group , 768.000, 512.000, 0.000, 1, 1, false, 20.000, 20.000, generateProperties( null ), onAddCallback );//"dialogintro"
			addSpriteToLayer(null, EnemyShip, Layer1Group , 2524.000, 1052.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spaceenemy1"
			addSpriteToLayer(null, EnemyShip, Layer1Group , 2300.000, 1180.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spaceenemy1"
			addSpriteToLayer(null, EnemyShip, Layer1Group , 2812.000, 1148.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spaceenemy1"
			addSpriteToLayer(new DialogTrigger(1984.000, 992.000, "Looks like I've run into the station's\ndefense bots.\n\nLet's rock and roll.\n\n\n\n<Controls: Z to fire your beam>\n\n\n\n-Press SPACE to continue-"), DialogTrigger, Layer1Group , 1984.000, 992.000, 0.000, 1, 1, false, 10.000, 10.000, generateProperties( null ), onAddCallback );//"dialogenemies"
			addSpriteToLayer(null, EnemyTurret, Layer1Group , 3392.000, 704.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spaceenemy2"
			addSpriteToLayer(null, EnemyTurret, Layer1Group , 3392.000, 864.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spaceenemy2"
			addSpriteToLayer(null, EnemyTurret, Layer1Group , 3648.000, 768.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spaceenemy2"
			addSpriteToLayer(null, EnemyTurret, Layer1Group , 3872.000, 704.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spaceenemy2"
			addSpriteToLayer(null, EnemyTurret, Layer1Group , 3872.000, 864.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spaceenemy2"
			addSpriteToLayer(null, EnemyShip, Layer1Group , 4572.000, 1596.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spaceenemy1"
			addSpriteToLayer(null, EnemyShip, Layer1Group , 4860.000, 2012.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spaceenemy1"
			addSpriteToLayer(null, EnemyShip, Layer1Group , 5372.000, 1884.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spaceenemy1"
			addSpriteToLayer(null, EnemyTurret, Layer1Group , 4544.000, 2016.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spaceenemy2"
			addSpriteToLayer(null, EnemyTurret, Layer1Group , 4768.000, 1792.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spaceenemy2"
			addSpriteToLayer(null, EnemyTurret, Layer1Group , 4736.000, 1600.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spaceenemy2"
			addSpriteToLayer(null, EnemyShip, Layer1Group , 5052.000, 1564.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spaceenemy1"
			addSpriteToLayer(null, EnemyTurret, Layer1Group , 5120.000, 1920.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spaceenemy2"
			addSpriteToLayer(null, EnemyTurret, Layer1Group , 5408.000, 1568.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spaceenemy2"
			addSpriteToLayer(new DialogTrigger(5888.000, 2144.000, "This passage is a bit too\nnarrow for my ship...\n\nTime to activate the\nMinimizer Mk II.\n\n\n\n<Controls: X to shrink your ship>\n\n\n-Press SPACE to continue-"), DialogTrigger, Layer1Group , 5888.000, 2144.000, 0.000, 1, 1, false, 20.000, 4.000, generateProperties( null ), onAddCallback );//"dialogshrink"
			addSpriteToLayer(new DialogTrigger(5888.000, 2720.000, "All clear...\n\nLooks like some more\nbots up ahead, though.\n\n\n\n<Controls: X to unshrink your ship>\n\n\n\n-Press SPACE to continue-"), DialogTrigger, Layer1Group , 5888.000, 2720.000, 0.000, 1, 1, false, 20.000, 5.000, generateProperties( null ), onAddCallback );//"dialogunshrink"
			addSpriteToLayer(null, EnemyTurret, Layer1Group , 5440.000, 3104.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spaceenemy2"
			addSpriteToLayer(null, EnemyTurret, Layer1Group , 5440.000, 3552.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spaceenemy2"
			addSpriteToLayer(null, EnemyTurret, Layer1Group , 5664.000, 3200.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spaceenemy2"
			addSpriteToLayer(null, EnemyTurret, Layer1Group , 5664.000, 3456.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spaceenemy2"
			addSpriteToLayer(null, EnemyShip, Layer1Group , 5436.000, 3196.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spaceenemy1"
			addSpriteToLayer(null, EnemyShip, Layer1Group , 5436.000, 3388.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spaceenemy1"
			addSpriteToLayer(new Portal(4224.000, 3584.000, 900, 1000, "jungle1"), Portal, Layer1Group , 4224.000, 3584.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"space1tojungle1"
			addSpriteToLayer(new DialogTrigger(4352.000, 3424.000, "I've found some sort of\nsmall passageway at the\nend of this corridor.\n\nLet's see where it leads...\n\n\n<Shrink your ship to enter passages>\n\n\n\n-Press SPACE to continue-"), DialogTrigger, Layer1Group , 4352.000, 3424.000, 0.000, 1, 1, false, 5.000, 7.000, generateProperties( null ), onAddCallback );//"dialogportal"
			addSpriteToLayer(null, Spawner, Layer1Group , 4211.000, 1176.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spawner"
			addSpriteToLayer(null, Spawner, Layer1Group , 6162.000, 1813.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spawner"
		}

		public function generateObjectLinks(onAddCallback:Function = null):void
		{
		}

	}
}
