package  {
	import flash.geom.ColorTransform;
	import org.flixel.plugin.photonstorm.*;
	import org.flixel.*;
	public class Enemy extends FlxSprite {
		[Embed(source='../sfx/enemyhit.mp3')]
		private var SfxHurt:Class;
		[Embed(source='../sfx/enemydie.mp3')]
		private var SfxDie:Class;
		
		protected var _flashTimer:int = 0;
		protected var healthBar:FlxBar;
		
		public function Enemy(X:Number=0, Y:Number=0, graphic:Class=null) {
			super(X, Y);
			loadGraphic(graphic, false, true);
			health = maxHealth();
			healthBar = new FlxBar(0, 0, FlxBar.FILL_LEFT_TO_RIGHT, width, 10, this, "health", 0, health, true);
			healthBar.trackParent(0, -15);
			PlayState.State().levelGroup.add(healthBar);
		}
		
		public function maxHealth():Number {
			return 5;
		}
		
		public function damage():Number {
			return 5;
		}
		
		override public function update():void {
			super.update();

			// Hit flash.
			if (_flashTimer == 0) {
				_colorTransform = null;
				calcFrame();
			} else {
				--_flashTimer;
			}
		}
		
		override public function hurt(Damage:Number):void {
			super.hurt(Damage);
			
			// Hit flash.
			_colorTransform = new ColorTransform();
			_colorTransform.color = 0xaaaaaa;
			calcFrame();
			_flashTimer = 1;
			
			FlxG.play(SfxHurt);
			
			if (health <= 0) {
				FlxG.play(SfxDie);
				PlayState.State().explosions.Explode(x + width / 2, y + height / 2, 20);
			}
		}
		
		override public function kill():void {
			super.kill();
			healthBar.kill();
		}
		
		override public function destroy():void {
			super.destroy();
			healthBar.destroy();
		}
	}
}