package {
	import org.flixel.*;
	import org.flixel.plugin.photonstorm.*;
	import com.eclecticdesignstudio.motion.Actuate;
	import com.eclecticdesignstudio.motion.easing.*;
	
	public class Upgrade extends FlxSprite {
		[Embed(source='../mus/Upgrade Acquired.mp3')]
		private var MusUpgrade:Class;
		[Embed(source='../img/upgrade.png')]
		private var ImgUpgrade:Class;
		
		private var SoundUpgrade:FlxSound;
		
		public var upgrade:String;
		
		public function Upgrade(X:Number = 0, Y:Number = 0, whichUpgrade:String="") {
			super(X, Y);
			loadGraphic(ImgUpgrade, true, false, 48, 48);
			addAnimation("normal", [0, 1, 2, 3], 15, true);
			play("normal");
			upgrade = whichUpgrade;
			
			SoundUpgrade = new FlxSound();
			SoundUpgrade.loadEmbedded(MusUpgrade);
		}
		
		override public function update():void {
			super.update();
			
			// Collide with player.
			var collid:Boolean;
			if (PlayState.State().playerShip.shrunk) {
				collid = FlxG.collide(this, PlayState.State().playerShip);
			} else {
				collid = FlxCollision.pixelPerfectCheck(this, PlayState.State().playerShip);
			}
			if (collid) {
				exists = false;
				
				FlxG.music.volume = 0;
				SoundUpgrade.play(true);
				Actuate.tween(FlxG.music, 1, { volume: 1 }, false ).ease(Linear.easeNone).delay(2);
				
				// Give upgrade.
				switch (upgrade) {
					case "charge":
						PlayState.State().playerShip.weaponType = PlayerShip.WEAPON_CHARGE;
						PlayState.State().dialog("Charge Beam Upgrade Acquired\n\nYou can now charge your beam\n by holding the Z button.\n\nCharged shots can break through\n*red* blocks.\n\n\n\n\n-Press SPACE to continue-");
						break;
					case "green":
						PlayState.State().playerShip.weaponType = PlayerShip.WEAPON_GREEN;
						PlayState.State().dialog("Plasma Beam Upgrade Acquired\n\nYou can now break through\n*green* blocks.\n\n\n\n\n\n\n\n-Press SPACE to continue-");
						break;
				}
				
				return;
			}
		}
		
		public function unpause():void {
			FlxG.music.resume();
		}
	}
}