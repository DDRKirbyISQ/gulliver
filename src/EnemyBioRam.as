package  {
	import org.flixel.*;
	import org.flixel.plugin.photonstorm.*;
	
	public class EnemyBioRam extends Enemy {
		[Embed(source='../img/enemyship_1.png')]
		private var ImgShipDummy:Class;
		[Embed(source='../img/enemybio_2.png')]
		private var ImgShip:Class;
		[Embed(source='../img/enemyshot_2.png')]
		private var ImgBullet:Class;
		[Embed(source='../sfx/enemyshot_1.mp3')]
		private var SfxShot:Class;
		
		public var shotTimer:int = 0;
		
		public function EnemyBioRam(X:Number=0, Y:Number=0) {
			super(X, Y, ImgShipDummy);
			loadGraphic(ImgShip, true, false, 64, 64);
			addAnimation("normal", [0, 1, 2, 3], 15, true);
			play("normal");
			angularVelocity = 250 + FlxG.random() * 50;
			if (FlxG.random() < 0.5) {
				angularVelocity *= -1;
			}
			angle = FlxG.random() * 360;
		}
		
		override public function maxHealth():Number {
			return 30;
		}
		
		override public function damage():Number {
			return 20;
		}
		
		override public function update():void {
			super.update();

			var dist:FlxPoint = new FlxPoint((PlayState.State().playerShip.x + PlayState.State().playerShip.width / 2) - (x + width / 2),
											 (PlayState.State().playerShip.y + PlayState.State().playerShip.height / 2) - (y + height / 2));
			var distance:Number = Math.sqrt(dist.x * dist.x + dist.y * dist.y);
				
				if (distance < 500) {
					x += dist.x / distance * 3;
					y += dist.y / distance * 3;
					FlxG.collide(this, PlayState.State().currentLevel.tilemaps);
				}
			// Shoot.
			/*if (shotTimer == 0) {
				if (distance < 500) {
					dist.x /= distance;
					dist.y /= distance;
					dist.x *= 5;
					dist.y *= 5;
					for (var i:int = -1; i <= 1; ++i) {
						var bullet:EnemyBullet =
							PlayState.State().bullets.recycle(EnemyBullet) as EnemyBullet;
						bullet.loadGraphic(ImgBullet);
						bullet.reset(x + width / 2 - bullet.width / 2, y + height / 2 - bullet.height / 2);
						bullet.damage = 10;
						bullet.angularVelocity = 10;
						shotTimer = 120;
						var angle:Number = FlxVelocity.angleBetween(this, PlayState.State().playerShip, true);
						bullet.velocity = FlxVelocity.velocityFromAngle(angle + i * 30, 5);
					}
					FlxG.play(SfxShot);
				}				
			} else {
				shotTimer--;
			}*/
		}
	}
}