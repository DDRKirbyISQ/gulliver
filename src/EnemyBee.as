package  {
	import org.flixel.*;
	import org.flixel.plugin.photonstorm.*;
	
	public class EnemyBee extends Enemy {
		[Embed(source='../img/enemyship_1.png')]
		private var ImgShipDummy:Class;
		[Embed(source='../img/enemybug_1.png')]
		private var ImgShip:Class;
		[Embed(source='../img/enemyshot_3.png')]
		private var ImgBullet:Class;
		[Embed(source='../sfx/enemyshot_1.mp3')]
		private var SfxShot:Class;
		
		public var shotTimer:int = 0;
		
		public function EnemyBee(X:Number=0, Y:Number=0) {
			super(X, Y, ImgShipDummy);
			loadGraphic(ImgShip, true, true, 64, 64);
			addAnimation("normal", [0, 1, 2, 3], 60, true);
			play("normal");
		}
		
		override public function maxHealth():Number {
			return 30;
		}
		
		override public function damage():Number {
			return 15;
		}
		
		override public function update():void {
			super.update();
			
			var dist:FlxPoint = new FlxPoint((PlayState.State().playerShip.x + PlayState.State().playerShip.width / 2) - (x + width / 2),
											 (PlayState.State().playerShip.y + PlayState.State().playerShip.height / 2) - (y + height / 2));
			var distance:Number = Math.sqrt(dist.x * dist.x + dist.y * dist.y);

			if (dist.x < 0)
			facing = RIGHT;
			else
			facing = LEFT;
			
				if (distance < 500 && distance > 200) {
					x += dist.x / distance * 2;
					y += dist.y / distance * 2;
					FlxG.collide(this, PlayState.State().currentLevel.tilemaps);
					
					// collide with other bees
			// Collide with enemies.
			for each (var group:Object in PlayState.State().currentLevel.masterLayer.members) {
				if (group is FlxGroup) {
					for each (var enemy:Object in group.members) {
						if (enemy is Enemy) {
							FlxG.collide(this, enemy as Enemy);
						}
					}
				}
			}					
				}
			// Shoot.
			if (shotTimer == 0) {
				if (distance < 500) {
					dist.x /= distance;
					dist.y /= distance;
					dist.x *= 5;
					dist.y *= 5;
					for (var i:int = 0; i <= 2; ++i) {
						var bullet:EnemyBullet =
							PlayState.State().bullets.recycle(EnemyBullet) as EnemyBullet;
						bullet.loadGraphic(ImgBullet);
						bullet.reset(x + width / 2 - bullet.width / 2, y + height / 2 - bullet.height / 2);
						bullet.damage = 10;
						shotTimer = 100;
						var angle:Number = FlxVelocity.angleBetween(this, PlayState.State().playerShip, true);
						bullet.velocity = FlxVelocity.velocityFromAngle(angle, 4 + i * 2);
					}
					FlxG.play(SfxShot);
				}				
			} else {
				shotTimer--;
			}
		}
	}
}