package  {
	import org.flixel.*;
	import org.flixel.plugin.photonstorm.*;
	
	public class EnemyBullet extends FlxSprite {
		
		public var damage:Number;
		
		public function EnemyBullet(X:Number=0, Y:Number=0, SimpleGraphic:Class=null) {
			super(X, Y, SimpleGraphic);
		}
		
		override public function reset(X:Number, Y:Number):void {
			super.reset(X, Y);
			angularVelocity = 0;
		}
		
		override public function update():void {
			moves = false;
			super.update();
			
			x += velocity.x;
			y += velocity.y;
			angle += angularVelocity;

			// Collide with player.
			var collid:Boolean;
			if (PlayState.State().playerShip.shrunk) {
				collid = FlxG.overlap(this, PlayState.State().playerShip);
			} else {
				//collid = FlxG.overlap(this, PlayState.State().playerShip);
				collid = FlxCollision.pixelPerfectCheck(this, PlayState.State().playerShip);
			}
			if (collid) {
				if (PlayState.State().playerShip.exists && PlayState.State().playerShip.invincibilityTimer == 0) {
					PlayState.State().playerShip.hurt(damage);
					exists = false;
					return;
				}
			}
			
			// Collide with terrain.
			if (FlxG.collide(this, PlayState.State().currentLevel.tilemaps)) {
				exists = false;
			}
		}
	}
}