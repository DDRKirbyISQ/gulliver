//Code generated with DAME. http://www.dambots.com

package 
{
	import org.flixel.*;
	import flash.utils.Dictionary;
	public class Level_Cave1 extends BaseLevel
	{
		//Embedded media...
		[Embed(source="../map/mapCSV_Cave1_Map2.csv", mimeType="application/octet-stream")] public var CSV_Map2:Class;
		[Embed(source="../map/tiles_cave.png")] public var Img_Map2:Class;

		//Tilemaps
		public var layerMap2:FlxTilemap;

		//Sprites
		public var Layer1Group:FlxGroup = new FlxGroup;

		//Properties


		public function Level_Cave1(addToStage:Boolean = true, onAddCallback:Function = null, parentObject:Object = null)
		{
			// Generate maps.
			var properties:Array = [];
			var tileProperties:Dictionary = new Dictionary;

			properties = generateProperties( null );
			layerMap2 = addTilemap( CSV_Map2, Img_Map2, 0.000, 0.000, 32, 32, 1.000, 1.000, false, 1, 1, properties, onAddCallback );

			//Add layers to the master group in correct order.
			masterLayer.add(Layer1Group);
			masterLayer.add(layerMap2);

			if ( addToStage )
				createObjects(onAddCallback, parentObject);

			boundsMinX = 0;
			boundsMinY = 0;
			boundsMaxX = 6400;
			boundsMaxY = 3200;
			bgColor = 0xff777777;
		}

		override public function createObjects(onAddCallback:Function = null, parentObject:Object = null):void
		{
			addSpritesForLayerLayer1(onAddCallback);
			generateObjectLinks(onAddCallback);
			if ( parentObject != null )
				parentObject.add(masterLayer);
			else
				FlxG.state.add(masterLayer);
		}

		public function addSpritesForLayerLayer1(onAddCallback:Function = null):void
		{
			addSpriteToLayer(new BigPortal(384.000, 2528.000, 2340, 2854, "jungle1"), BigPortal, Layer1Group , 384.000, 2528.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"cave1tojungle1"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 1664.000, 2656.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 1664.000, 2688.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 1632.000, 2720.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 1664.000, 2720.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 1664.000, 2752.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 1632.000, 2784.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 1632.000, 2816.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 1664.000, 2816.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 1664.000, 2784.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 1632.000, 2720.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 1632.000, 2752.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 1632.000, 2688.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 1696.000, 2656.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 1632.000, 2656.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 1696.000, 2688.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 1696.000, 2720.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 1696.000, 2752.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 1696.000, 2784.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 1696.000, 2816.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 3360.000, 1472.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 3360.000, 1440.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 3360.000, 1504.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 3360.000, 1408.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 3360.000, 1344.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 3360.000, 1376.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 3360.000, 1312.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 3392.000, 1312.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 3392.000, 1344.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 3392.000, 1376.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 3392.000, 1376.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 3392.000, 1440.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 3392.000, 1376.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 3392.000, 1408.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 3392.000, 1472.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 3392.000, 1472.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 3392.000, 1504.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, Spawner, Layer1Group , 2080.000, 1664.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spawner"
			addSpriteToLayer(null, LaserRight, Layer1Group , 1632.000, 1632.000, 0.000, 1, 1, false, 1.000, 7.000, generateProperties( null ), onAddCallback );//"laserrigh"
			addSpriteToLayer(new DialogTrigger(1824.000, 1600.000, "Well, no turning back now, I guess...\n\n\n\n\n\n\n\n\n\n\n-Press SPACE to continue-"), DialogTrigger, Layer1Group , 1824.000, 1600.000, 0.000, 1, 1, false, 4.000, 10.000, generateProperties( null ), onAddCallback );//"dialogcaveboss"
			addSpriteToLayer(null, Boss2Trigger, Layer1Group , 2592.000, 1024.000, 0.000, 1, 1, false, 10.000, 50.000, generateProperties( null ), onAddCallback );//"boss2"
			addSpriteToLayer(new DialogTrigger(576.000, 2432.000, "This cavern must measure\non the order of mere centimeters!\n\nSuch is the power of the\nMinimizer Mk II...\n\n\n\n\n\n\n-Press SPACE to continue-"), DialogTrigger, Layer1Group , 576.000, 2432.000, 0.000, 1, 1, false, 11.000, 11.000, generateProperties( null ), onAddCallback );//"dialogcave"
		}

		public function generateObjectLinks(onAddCallback:Function = null):void
		{
		}

	}
}
