package {
	import org.flixel.*;
	import org.flixel.plugin.photonstorm.*;
	import org.flixel.plugin.photonstorm.FX.StarfieldFX;
	
	public class MenuState extends FlxState {
		[Embed(source='../mus/Gulliver.mp3')]
		private var MusGulliver:Class;
		[Embed(source='../sfx/start.mp3')]
		private var SfxStart:Class;
		[Embed(source='../sfx/menu.mp3')]
		private var SfxMenu:Class;
		[Embed(source='../img/background_menu.png')]
		private var ImgBackdrop:Class;
		
		public var starfield:StarfieldFX = FlxSpecialFX.starfield();
		public var stars:FlxSprite;
		public var backdrop:FlxSprite;
		
		public var countdown:int;
		public var started:Boolean;
        public var locked:Boolean;
        
        public var difficultyText:FlxText;

		public function MenuState():void {
		}
		
		override public function create():void {
			super.create();
            locked = false;
			
			countdown = 0;
			started = false;
			
			FlxG.playMusic(MusGulliver);

			// Backdrop.
			backdrop = new FlxSprite(0, 0, ImgBackdrop);
			add(backdrop);
			backdrop.scrollFactor.x = 0;
			backdrop.scrollFactor.y = 0;
			
			// Starfield.
			if (FlxG.getPlugin(FlxSpecialFX) == null) {
				FlxG.addPlugin(new FlxSpecialFX);
			}
			stars = starfield.create(0, 0, FlxG.width, FlxG.height, 125);
			starfield.setStarSpeed( -0.4, 0);
			starfield.setStarDepthColors(10, 0xff5566ff, 0xffddddff);
			starfield.setBackgroundColor(0x00);
			stars.scale.x = 2;
			stars.scale.y = 2;
			stars.antialiasing = true;
			add(stars);
			stars.scrollFactor.x = 0;
			stars.scrollFactor.y = 0;

			// Text.
			add(new FlxText(0, FlxG.height / 4, FlxG.width, 'Gulliver').setFormat(null, 64, 0xffffff, 'center', 0x000000));
			add(new FlxText(0, FlxG.height / 4 + 75, FlxG.width, 'v1.02').setFormat(null, 16, 0xffffff, 'center', 0x000000));
			add(new FlxText(0, FlxG.height / 4 + 100, FlxG.width, 'by DDRKirby(ISQ)').setFormat(null, 16, 0xffffff, 'center', 0x000000));
			difficultyText = new FlxText(300, (FlxG.height * 3 / 4 - 50), FlxG.width, 'Difficulty: ').setFormat(null, 16, 0xffffff, 'left');
            add(difficultyText);
			add(new FlxText(0, (FlxG.height * 3 / 4), FlxG.width, 'Press SPACE to start').setFormat(null, 16, 0xffff00, 'center'));
		}
		
		override public function update():void {
            switch (Difficulty.difficulty) {
                case Difficulty.EASY:
                    difficultyText.text = 'Difficulty:    Easy     >';
                    break;
                case Difficulty.NORMAL:
                    difficultyText.text = 'Difficulty: < Normal >';
                    break;
                case Difficulty.HARD:
                    difficultyText.text = 'Difficulty: <  Hard   ';
                    break;
            }
            
            if (FlxG.keys.justPressed("LEFT") && !locked) {
                if (Difficulty.difficulty == 0) {
                } else {
                Difficulty.difficulty--;
				FlxG.play(SfxMenu);
				}
            }
            
            if (FlxG.keys.justPressed("RIGHT") && !locked) {
                if (Difficulty.difficulty == 2) {
                } else {
                Difficulty.difficulty = (Difficulty.difficulty + 1);
				FlxG.play(SfxMenu);
				}
            }
            
			if (FlxG.keys.justPressed("SPACE")) {
				FlxG.music.fadeOut(2);
				FlxG.play(SfxStart);
				FlxG.fade(0xff000000, 2, done);
                locked = true;
			}
			
			countdown--;
			if (started && countdown == 0) {
				realdone();
			}
			
		}
		
		public function done():void {
			countdown = 60;
			started = true;
		}
		
		public function realdone():void {
			FlxSpecialFX.clear();
			FlxG.switchState(new IntroState());
		}
		
		override public function destroy():void {
			super.destroy();
		}
	}
}