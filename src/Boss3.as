package  {
	import adobe.utils.ProductManager;
	import org.flixel.*;
	import org.flixel.plugin.photonstorm.*;
	import com.eclecticdesignstudio.motion.Actuate;
	import com.eclecticdesignstudio.motion.easing.equations.LinearEaseNone;
	import com.eclecticdesignstudio.motion.easing.Linear;
	
	public class Boss3 extends Enemy {
		[Embed(source='../img/boss_3_dummy.png')]
		private var ImgShipDummy:Class;
		[Embed(source='../img/boss_3.png')]
		private var ImgShip:Class;
		[Embed(source='../img/enemyshot_2.png')]
		private var ImgBullet:Class;
		[Embed(source='../img/enemyshot_2.png')]
		private var ImgBullet2:Class;
		[Embed(source='../sfx/enemyshot_1.mp3')]
		private var SfxShot:Class;
		[Embed(source='../mus/Flesh.mp3')]
		private var Mus:Class;
		
		public var shotTimer:int = 0;
		public var shotTimer2:int = 0;
		public var angle2:Number = 0;
		
		public function Boss3(X:Number=0, Y:Number=0) {
			super(X, Y, ImgShipDummy);
			loadGraphic(ImgShip, true, false, 256, 256);
			addAnimation("normal", [0, 1, 2, 3], 15, true);
			play("normal");
			angularVelocity = 150 + FlxG.random() * 50;
			if (FlxG.random() < 0.5) {
				angularVelocity *= -1;
			}
			angle = FlxG.random() * 360;
		}
		
		override public function maxHealth():Number {
			return 300;
		}
		
		override public function damage():Number {
			return 25;
		}
		
		override public function update():void {
			super.update();
			
			var dist:FlxPoint = new FlxPoint((PlayState.State().playerShip.x + PlayState.State().playerShip.width / 2) - (x + width / 2),
											 (PlayState.State().playerShip.y + PlayState.State().playerShip.height / 2) - (y + height / 2));
			var distance:Number = Math.sqrt(dist.x * dist.x + dist.y * dist.y);

				if (distance > 350) {
					x += dist.x / distance * 2;
					y += dist.y / distance * 2;
					FlxG.collide(this, PlayState.State().currentLevel.tilemaps);
				}
			// Shoot.
			if (shotTimer == 0) {
					var angle:Number = FlxVelocity.angleBetween(this, PlayState.State().playerShip, false);
					
					if (FlxG.random() < 0.5) {
				for (var i:int = 0; i <= 12; ++i) {
					var bullet:EnemyBullet =
						PlayState.State().bullets.recycle(EnemyBullet) as EnemyBullet;
					bullet.loadGraphic(ImgBullet);
					bullet.reset(x + width / 2 - bullet.width / 2, y + height / 2 - bullet.height / 2);
					bullet.damage = 15;
					//bullet.velocity = FlxVelocity.velocityFromAngle(angle + 360 / 8 * i, 4);
					bullet.velocity.x = (4) * (Math.cos(angle + 360 / 8 * i));
					bullet.velocity.y = (4) * (Math.sin(angle + 360 / 8 * i));
				}
					} else {
				
				for (i = 0; i <= 3; ++i) {
					bullet =
						PlayState.State().bullets.recycle(EnemyBullet) as EnemyBullet;
					bullet.loadGraphic(ImgBullet2);
					bullet.reset(x + width / 2 - bullet.width / 2, y + height / 2 - bullet.height / 2);
					bullet.damage = 15;
					//bullet.velocity = FlxVelocity.velocityFromAngle(angle, 3 + i * 1.5);
					bullet.velocity.x = (3 + i * 1.25) * (Math.cos(angle));
					bullet.velocity.y = (3 + i * 1.25) * (Math.sin(angle));
				}
					}
				shotTimer = 40;
				FlxG.play(SfxShot);
			} else {
				shotTimer--;
			}
			
			if (shotTimer2 == 0) {
					bullet =
						PlayState.State().bullets.recycle(EnemyBullet) as EnemyBullet;
					bullet.loadGraphic(ImgBullet2);
					bullet.reset(x + width / 2 - bullet.width / 2, y + height / 2 - bullet.height / 2);
					bullet.damage = 15;
					bullet.velocity = FlxVelocity.velocityFromAngle(angle2, 3);
					angle2 += 35;
				
				shotTimer2 = 9;
				FlxG.play(SfxShot);
			} else {
				shotTimer2--;
			}
		}
		
		override public function hurt(Damage:Number):void {
			super.hurt(Damage);
			
			if (health <= 0) {
				// Spawn upgrade.
				PlayState.State().currentLevel.masterLayer.members[0].add(new Upgrade(x + width / 2 - 24, y + width / 2 - 24, "green"));
				
				// Flash.
				FlxG.flash(0xffffffff, 1);
				
				// Shake.
				FlxG.shake();
				
				// music.
				Actuate.tween(FlxG.music, 1, { volume: 0 }, true).ease(Linear.easeNone).onComplete(playMusic);
			}
		}
		
		public function playMusic():void {
			FlxG.playMusic(Mus);
		}
		
		override public function kill():void {
			super.kill();
		}
	}
}