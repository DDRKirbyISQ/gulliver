package  {
	import adobe.utils.ProductManager;
	import org.flixel.*;
	import org.flixel.plugin.photonstorm.*;
	
	public class EnemyBird2 extends Enemy {
		[Embed(source='../img/enemyship_1.png')]
		private var ImgShipDummy:Class;
		[Embed(source='../img/enemybird_2.png')]
		private var ImgShip:Class;
		[Embed(source='../img/enemyshot_5.png')]
		private var ImgBullet:Class;
		[Embed(source='../sfx/enemyshot_1.mp3')]
		private var SfxShot:Class;
		
		public var shotTimer:int = 0;
		public var shotsInARow:int = 0;
		
		public function EnemyBird2(X:Number=0, Y:Number=0) {
			super(X, Y, ImgShipDummy);
			loadGraphic(ImgShip, true, true, 64, 64);
			addAnimation("normal", [0, 1, 2, 3, 2, 1], 30, true);
			play("normal");
		}
		
		override public function maxHealth():Number {
			return 20;
		}
		
		override public function damage():Number {
			return 10;
		}
		
		override public function update():void {
			super.update();
			
			var dist:FlxPoint = new FlxPoint((PlayState.State().playerShip.x + PlayState.State().playerShip.width / 2) - (x + width / 2),
											 (PlayState.State().playerShip.y + PlayState.State().playerShip.height / 2) - (y + height / 2));
			var distance:Number = Math.sqrt(dist.x * dist.x + dist.y * dist.y);

			if (dist.x < 0)
			facing = RIGHT;
			else
			facing = LEFT;
			
				if (distance > 300 && distance < 500) {
					x += dist.x / distance * 2;
					y += dist.y / distance * 2;
					FlxG.collide(this, PlayState.State().currentLevel.tilemaps);
				}
					// collide with other bees
			// Collide with enemies.
			for each (var group:Object in PlayState.State().currentLevel.masterLayer.members) {
				if (group is FlxGroup) {
					for each (var enemy:Object in group.members) {
						if (enemy is Enemy) {
							FlxG.collide(this, enemy as Enemy);
						}
					}
				}
			}					
				
			// Shoot.
			if (shotTimer == 0) {
                if (distance < 350) {
					var angle:Number = FlxVelocity.angleBetween(this, PlayState.State().playerShip, true);
					var bullet:EnemyBullet =
						PlayState.State().bullets.recycle(EnemyBullet) as EnemyBullet;
					bullet.loadGraphic(ImgBullet);
					bullet.reset(x + width / 2 - bullet.width / 2, y + height / 2 - bullet.height / 2);
					bullet.damage = 10;
					bullet.angularVelocity = 10;
                    if (FlxG.random() < 0.5) {
                        bullet.angularVelocity *= -1;
                    }
					bullet.velocity = FlxVelocity.velocityFromAngle(angle + (FlxG.random() - 0.5) * 60, 5);
					
					if (shotsInARow == 5) {
						shotTimer = 120;
						shotsInARow = 0;
					} else {
						shotTimer = 5;
						shotsInARow++;
					}
                    FlxG.play(SfxShot);
                }

			} else {
				shotTimer--;
			}
		}
	}
}