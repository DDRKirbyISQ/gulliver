package  {
	import org.flixel.*;
	import com.eclecticdesignstudio.motion.Actuate;
	import com.eclecticdesignstudio.motion.easing.Linear;
	
	public class BigPortal extends FlxSprite {
		[Embed(source='../img/bigportal.png')]
		private var ImgPortal:Class;
		[Embed(source='../sfx/portal.mp3')]
		private var SfxPortal:Class;
		
		public var destX:Number;
		public var destY:Number;
		public var map:String;
		
		public function BigPortal(X:Number, Y:Number, DestX:Number, DestY:Number, Map:String) {
			super(X, Y, ImgPortal);
			offset.x = width / 4;
			offset.y = height / 2;
			width /= 2;
			height /= 2;
			
			map = Map;
			destX = DestX;
			destY = DestY;
		}
		
		override public function update():void {
			super.update();
			
			var ship:PlayerShip = PlayState.State().playerShip;
			if (FlxG.overlap(this, ship)) {
				FlxG.play(SfxPortal);

				PlayState.State().pause();
				Actuate.tween(PlayState.State().fader, 0.5, { alpha:1 } ).ease(Linear.easeNone).onComplete(finish);
				FlxG.music.fadeOut(0.5);
			}
		}
		
		public function finish():void {
			var shrink:Boolean = false;
			switch (map) {
				case "space1":
					PlayState.State().switchLevel(new Level_Space1(true, null, PlayState.State().levelGroup), true, shrink);
					break;
				case "space2":
					PlayState.State().switchLevel(new Level_Space2(true, null, PlayState.State().levelGroup), true, shrink);
					break;
				case "micro1":
					PlayState.State().switchLevel(new Level_Micro1(true, null, PlayState.State().levelGroup), true, shrink);
					break;
				case "jungle1":
					PlayState.State().switchLevel(new Level_Jungle1(true, null, PlayState.State().levelGroup), true, shrink);
					break;
				case "jungle2":
					PlayState.State().switchLevel(new Level_Jungle2(true, null, PlayState.State().levelGroup), true, shrink);
					break;
				case "cave1":
					PlayState.State().switchLevel(new Level_Cave1(true, null, PlayState.State().levelGroup), true, shrink);
					break;					
				case "cave2":
					PlayState.State().switchLevel(new Level_Cave2(true, null, PlayState.State().levelGroup), true, shrink);
					break;
				default:
					break;
			}

			
			var ship:PlayerShip = PlayState.State().playerShip;

			ship.x = destX;
			ship.y = destY;
			ship.charge = 0;
			ship.SoundCharged.volume = 0;


			PlayState.State().respawnX = destX;
			PlayState.State().respawnY = destY;
			PlayState.State().unpause();
			Actuate.tween(PlayState.State().fader, 0.5, { alpha:0 } ) .ease(Linear.easeNone);
		}
	}
}