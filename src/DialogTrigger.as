package  {
	import org.flixel.*;
	import org.flixel.plugin.photonstorm.*;
	
	public class DialogTrigger extends FlxSprite {
		[Embed(source='../img/dialog.png')]
		private var ImgRect:Class;
		
		public var triggerText:String;
		
		public function DialogTrigger(X:Number=0, Y:Number=0, text:String="") {
			super(X, Y, ImgRect);
			triggerText = text;
			visible = false;
			//TODO: disable debug display!
			
			if (PlayState.State().seenDialogs[triggerText] == "yes") {
				exists = false;
			}
		}
		
		override public function update():void {
			super.update();
			
			if (FlxG.overlap(this, PlayState.State().playerShip)) {
				PlayState.State().dialog(triggerText);
				exists = false;
				
				// Mark as seen.
				PlayState.State().seenDialogs[triggerText] = "yes";
			}
		}
	}
}