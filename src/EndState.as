package  {
	import org.flixel.*;
	

	public class EndState extends FlxState {
		[Embed(source='../mus/Gulliver (Reprise).mp3')]
		private var Mus:Class;
		[Embed(source='../sfx/start.mp3')]
		private var Sfx:Class;
		
		private const text1:String = "Gulliver";
		private const text2:String = "created by DDRKirby(ISQ)";
		private const text3:String = "in 48 hours";
		private const text4:String = "for Ludum Dare 23";
		private const text5:String = "Thank you for playing!";
		private const text6:String = "Press SPACE to continue";
		
		public var t1:FlxText;
		public var t2:FlxText;
		public var t3:FlxText;
		public var t4:FlxText;
		public var t5:FlxText;
		public var t6:FlxText;
		
		public var timer:int;
		
		public var fading:Boolean;
		
		public function EndState() {
			
		}
		override public function create():void {
			super.create();
			
			fading = false;
			
			timer = 0;

			// Text.
			add(t1=new FlxText(0, FlxG.height / 4, FlxG.width, text1).setFormat(null, 16, 0xffffff, 'center', 0x000000));
			add(t2=new FlxText(0, FlxG.height / 4 + 50, FlxG.width, text2).setFormat(null, 16, 0xffffff, 'center', 0x000000));
			add(t3=new FlxText(0, FlxG.height / 4 + 100, FlxG.width, text3).setFormat(null, 16, 0xffffff, 'center', 0x000000));
			add(t4=new FlxText(0, FlxG.height / 4 + 150, FlxG.width, text4).setFormat(null, 16, 0xffffff, 'center', 0x000000));
			add(t5=new FlxText(0, FlxG.height / 4 + 200, FlxG.width, text5).setFormat(null, 16, 0xffffff, 'center', 0x000000));
			add(t6=new FlxText(0, FlxG.height / 4 + 250, FlxG.width, text6).setFormat(null, 16, 0xffff00, 'center', 0x000000));
			
			t1.alpha = 0;
			t2.alpha = 0;
			t3.alpha = 0;
			t4.alpha = 0;
			t5.alpha = 0;
			t6.alpha = 0;
			
			FlxG.playMusic(Mus);
		}
		
		override public function update():void {
			super.update();
			
			var fade:Number = 0.01;
			
			timer++;
			
			if (fading) {
				t1.alpha -= fade;
				t2.alpha -= fade;
				t3.alpha -= fade;
				t4.alpha -= fade;
				t5.alpha -= fade;
				t6.alpha -= fade;
			} else {
			if (timer > 60)
				t1.alpha += fade;
			if (timer > 180)
				t2.alpha += fade;
			if (timer > 300)
				t3.alpha += fade;
			if (timer > 420)
				t4.alpha += fade;
			if (timer > 540)
				t5.alpha += fade;
			if (timer > 660)
				t6.alpha += fade;
			}
			
			if (FlxG.keys.justPressed("SPACE") && timer > 660) {
				fading = true;
				timer = 0;
				FlxG.play(Sfx);
				FlxG.music.fadeOut(2);
			}
			
			if (timer > 180 && fading) {
				FlxG.switchState(new MenuState());
			}
		}		
	}
}