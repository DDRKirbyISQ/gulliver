//Code generated with DAME. http://www.dambots.com

package 
{
	import org.flixel.*;
	import flash.utils.Dictionary;
	public class Level_TestLevel extends BaseLevel
	{
		//Embedded media...
		[Embed(source="../map/mapCSV_TestLevel_Map2.csv", mimeType="application/octet-stream")] public var CSV_Map2:Class;
		[Embed(source="../map/tiles_space.png")] public var Img_Map2:Class;

		//Tilemaps
		public var layerMap2:FlxTilemap;

		//Sprites
		public var Layer1Group:FlxGroup = new FlxGroup;

		//Properties


		public function Level_TestLevel(addToStage:Boolean = true, onAddCallback:Function = null, parentObject:Object = null)
		{
			// Generate maps.
			var properties:Array = [];
			var tileProperties:Dictionary = new Dictionary;

			properties = generateProperties( null );
			tileProperties[0]=generateProperties( { name:"Item0", value:"" }, null );
			properties.push( { name:"%DAME_tiledata%", value:tileProperties } );
			layerMap2 = addTilemap( CSV_Map2, Img_Map2, 0.000, 0.000, 32, 32, 1.000, 1.000, true, 1, 1, properties, onAddCallback );

			//Add layers to the master group in correct order.
			masterLayer.add(Layer1Group);
			masterLayer.add(layerMap2);

			if ( addToStage )
				createObjects(onAddCallback, parentObject);

			boundsMinX = 0;
			boundsMinY = 0;
			boundsMaxX = 1600;
			boundsMaxY = 1600;
			bgColor = 0xff777777;
		}

		override public function createObjects(onAddCallback:Function = null, parentObject:Object = null):void
		{
			addSpritesForLayerLayer1(onAddCallback);
			generateObjectLinks(onAddCallback);
			if ( parentObject != null )
				parentObject.add(masterLayer);
			else
				FlxG.state.add(masterLayer);
		}

		public function addSpritesForLayerLayer1(onAddCallback:Function = null):void
		{
			addSpriteToLayer(null, EnemyShip, Layer1Group , 1164.000, 1145.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spaceenemy1"
			addSpriteToLayer(null, EnemyShip, Layer1Group , 984.000, 1333.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spaceenemy1"
			addSpriteToLayer(null, EnemyShip, Layer1Group , 716.000, 1167.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spaceenemy1"
			addSpriteToLayer(null, EnemyShip, Layer1Group , 1062.000, 2515.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spaceenemy1"
			addSpriteToLayer(null, EnemyShip, Layer1Group , 1152.000, 2057.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spaceenemy1"
			addSpriteToLayer(null, EnemyShip, Layer1Group , 1370.000, 2391.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spaceenemy1"
			addSpriteToLayer(null, EnemyShip, Layer1Group , 1684.000, 2117.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spaceenemy1"
			addSpriteToLayer(null, EnemyShip, Layer1Group , 1816.000, 2627.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spaceenemy1"
			addSpriteToLayer(null, EnemyTurret, Layer1Group , 834.000, 287.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spaceenemy2"
			addSpriteToLayer(null, EnemyTurret, Layer1Group , 376.000, 209.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spaceenemy2"
			addSpriteToLayer(null, EnemyTurret, Layer1Group , 622.000, 255.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spaceenemy2"
			addSpriteToLayer(new DialogTrigger(160.000, 928.000, "U.S.S. Gulliver reporting in.\n\nI've successfully infiltrated\nthe station.\n\nSomething tells me this isn't\ngoing to be easy, but we'll\nfind out soon enough...\n\n<Controls: Arrow keys to move>\n\n-Press SPACE to continue-"), DialogTrigger, Layer1Group , 160.000, 928.000, 0.000, 1, 1, false, 2.000, 5.000, generateProperties( null ), onAddCallback );//"dialogintro"
			addSpriteToLayer(new Portal(736.000, 704.000, 900, 1000, "jungle1"), Portal, Layer1Group , 736.000, 704.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"space1tojungle1"
			addSpriteToLayer(new Portal(448.000, 704.000, 900, 1000, "jungle1"), Portal, Layer1Group , 448.000, 704.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"space1tojungle1"
			addSpriteToLayer(null, Block, Layer1Group , 544.000, 512.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"block"
			addSpriteToLayer(null, Block, Layer1Group , 576.000, 512.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"block"
			addSpriteToLayer(null, Block, Layer1Group , 832.000, 544.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"block"
			addSpriteToLayer(null, Block, Layer1Group , 832.000, 576.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"block"
			addSpriteToLayer(null, Block, Layer1Group , 832.000, 608.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"block"
			addSpriteToLayer(null, Block, Layer1Group , 864.000, 608.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"block"
			addSpriteToLayer(null, Block, Layer1Group , 896.000, 608.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"block"
			addSpriteToLayer(null, Block, Layer1Group , 896.000, 576.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"block"
			addSpriteToLayer(null, Block, Layer1Group , 896.000, 544.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"block"
			addSpriteToLayer(null, Block, Layer1Group , 864.000, 544.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"block"
			addSpriteToLayer(null, EnemyBioRam, Layer1Group , 1120.000, 832.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"bioram"
			addSpriteToLayer(null, EnemyBioRam, Layer1Group , 1248.000, 704.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"bioram"
			addSpriteToLayer(null, EnemyBioRam, Layer1Group , 928.000, 960.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"bioram"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 608.000, 1344.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 640.000, 1344.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 672.000, 1344.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 672.000, 1280.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 640.000, 1248.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 672.000, 960.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 736.000, 960.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 736.000, 960.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 672.000, 992.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 736.000, 992.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, EnemyBee, Layer1Group , 256.000, 224.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"bee"
			addSpriteToLayer(null, EnemyBee, Layer1Group , 192.000, 288.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"bee"
			addSpriteToLayer(null, EnemyBee, Layer1Group , 512.000, 192.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"bee"
			addSpriteToLayer(new LaserRight(704.000, 512.000, true), LaserRight, Layer1Group , 704.000, 512.000, 0.000, 1, 1, false, 1.000, 5.000, generateProperties( null ), onAddCallback );//"laserrightactive"
		}

		public function generateObjectLinks(onAddCallback:Function = null):void
		{
		}

	}
}
