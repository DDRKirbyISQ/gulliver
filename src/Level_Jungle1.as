//Code generated with DAME. http://www.dambots.com

package 
{
	import org.flixel.*;
	import flash.utils.Dictionary;
	public class Level_Jungle1 extends BaseLevel
	{
		//Embedded media...
		[Embed(source="../map/mapCSV_Jungle1_Map2.csv", mimeType="application/octet-stream")] public var CSV_Map2:Class;
		[Embed(source="../map/tiles_jungle.png")] public var Img_Map2:Class;

		//Tilemaps
		public var layerMap2:FlxTilemap;

		//Sprites
		public var Layer1Group:FlxGroup = new FlxGroup;

		//Properties


		public function Level_Jungle1(addToStage:Boolean = true, onAddCallback:Function = null, parentObject:Object = null)
		{
			// Generate maps.
			var properties:Array = [];
			var tileProperties:Dictionary = new Dictionary;

			properties = generateProperties( null );
			layerMap2 = addTilemap( CSV_Map2, Img_Map2, 0.000, 0.000, 32, 32, 1.000, 1.000, false, 1, 1, properties, onAddCallback );

			//Add layers to the master group in correct order.
			masterLayer.add(Layer1Group);
			masterLayer.add(layerMap2);

			if ( addToStage )
				createObjects(onAddCallback, parentObject);

			boundsMinX = 0;
			boundsMinY = 0;
			boundsMaxX = 6400;
			boundsMaxY = 3200;
			bgColor = 0xff777777;
		}

		override public function createObjects(onAddCallback:Function = null, parentObject:Object = null):void
		{
			addSpritesForLayerLayer1(onAddCallback);
			generateObjectLinks(onAddCallback);
			if ( parentObject != null )
				parentObject.add(masterLayer);
			else
				FlxG.state.add(masterLayer);
		}

		public function addSpritesForLayerLayer1(onAddCallback:Function = null):void
		{
			addSpriteToLayer(null, Spawner, Layer1Group , 2256.000, 1368.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spawner"
			addSpriteToLayer(new BigPortal(672.000, 800.000, 4315, 3608, "space1"), BigPortal, Layer1Group , 672.000, 800.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"jungle1tospace1"
			addSpriteToLayer(null, EnemyBird, Layer1Group , 1600.000, 640.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"jungleenemy1"
			addSpriteToLayer(null, EnemyBird, Layer1Group , 2336.000, 864.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"jungleenemy1"
			addSpriteToLayer(new DialogTrigger(1056.000, 640.000, "Seems like I'm in some\nsort of miniature nature\nbiome, with vegetation and wildlife...\n\nFascinating...\n\n\n\n\n\n\n-Press SPACE to continue-"), DialogTrigger, Layer1Group , 1056.000, 640.000, 0.000, 1, 1, false, 5.000, 12.000, generateProperties( null ), onAddCallback );//"dialogjungle"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 2368.000, 1920.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 2368.000, 1920.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 2368.000, 1984.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 2368.000, 1952.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 2368.000, 2016.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 2368.000, 2048.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 2400.000, 2048.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 2400.000, 2016.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 2400.000, 1984.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 2400.000, 1920.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 2400.000, 1952.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 2432.000, 1920.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 2432.000, 1952.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 2432.000, 1984.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 2432.000, 2016.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(null, BlockCharged, Layer1Group , 2432.000, 2048.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"redblock"
			addSpriteToLayer(new DialogTrigger(2016.000, 1856.000, "Those red blocks don't\nlook like they'll be\naffected by my beam...\n\nMaybe if I had a\nstronger weapon...\n\n\n\n\n\n-Press SPACE to continue-"), DialogTrigger, Layer1Group , 2016.000, 1856.000, 0.000, 1, 1, false, 5.000, 10.000, generateProperties( null ), onAddCallback );//"dialogredblock"
			addSpriteToLayer(new Portal(2400.000, 2816.000, 645, 2735, "cave1"), Portal, Layer1Group , 2400.000, 2816.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"jungle1tocave1"
			addSpriteToLayer(null, EnemyBird2, Layer1Group , 1312.000, 1920.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"jungleenemy2"
			addSpriteToLayer(null, EnemyBird2, Layer1Group , 832.000, 1952.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"jungleenemy2"
			addSpriteToLayer(null, EnemyBird2, Layer1Group , 1088.000, 2304.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"jungleenemy2"
			addSpriteToLayer(null, EnemyBird, Layer1Group , 960.000, 2816.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"jungleenemy1"
			addSpriteToLayer(null, EnemyBird2, Layer1Group , 1984.000, 2688.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"jungleenemy2"
			addSpriteToLayer(null, EnemyBird, Layer1Group , 1600.000, 2816.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"jungleenemy1"
			addSpriteToLayer(null, EnemyBird2, Layer1Group , 3200.000, 1888.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"jungleenemy2"
			addSpriteToLayer(null, EnemyBird, Layer1Group , 2912.000, 2016.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"jungleenemy1"
			addSpriteToLayer(new Portal(3680.000, 1984.000, 825, 741, "cave2"), Portal, Layer1Group , 3680.000, 1984.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"jungle1tocave2"
		}

		public function generateObjectLinks(onAddCallback:Function = null):void
		{
		}

	}
}
