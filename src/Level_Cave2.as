//Code generated with DAME. http://www.dambots.com

package 
{
	import org.flixel.*;
	import flash.utils.Dictionary;
	public class Level_Cave2 extends BaseLevel
	{
		//Embedded media...
		[Embed(source="../map/mapCSV_Cave2_Map2.csv", mimeType="application/octet-stream")] public var CSV_Map2:Class;
		[Embed(source="../map/tiles_cave.png")] public var Img_Map2:Class;

		//Tilemaps
		public var layerMap2:FlxTilemap;

		//Sprites
		public var Layer1Group:FlxGroup = new FlxGroup;

		//Properties


		public function Level_Cave2(addToStage:Boolean = true, onAddCallback:Function = null, parentObject:Object = null)
		{
			// Generate maps.
			var properties:Array = [];
			var tileProperties:Dictionary = new Dictionary;

			properties = generateProperties( null );
			layerMap2 = addTilemap( CSV_Map2, Img_Map2, 0.000, 0.000, 32, 32, 1.000, 1.000, false, 1, 1, properties, onAddCallback );

			//Add layers to the master group in correct order.
			masterLayer.add(Layer1Group);
			masterLayer.add(layerMap2);

			if ( addToStage )
				createObjects(onAddCallback, parentObject);

			boundsMinX = 0;
			boundsMinY = 0;
			boundsMaxX = 6400;
			boundsMaxY = 6400;
			bgColor = 0xff777777;
		}

		override public function createObjects(onAddCallback:Function = null, parentObject:Object = null):void
		{
			addSpritesForLayerLayer1(onAddCallback);
			generateObjectLinks(onAddCallback);
			if ( parentObject != null )
				parentObject.add(masterLayer);
			else
				FlxG.state.add(masterLayer);
		}

		public function addSpritesForLayerLayer1(onAddCallback:Function = null):void
		{
			addSpriteToLayer(new BigPortal(512.000, 544.000, 3642, 2020, "jungle1"), BigPortal, Layer1Group , 512.000, 544.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"cave2tojungle1"
			addSpriteToLayer(null, EnemyBee, Layer1Group , 2336.000, 2016.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"bee"
			addSpriteToLayer(null, EnemyBee, Layer1Group , 2848.000, 2240.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"bee"
			addSpriteToLayer(null, EnemyBee, Layer1Group , 3424.000, 2048.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"bee"
			addSpriteToLayer(null, EnemyBee, Layer1Group , 3200.000, 3200.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"bee"
			addSpriteToLayer(null, EnemyBee, Layer1Group , 2944.000, 3136.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"bee"
			addSpriteToLayer(new Portal(2432.000, 3264.000, 715, 2551, "micro1"), Portal, Layer1Group , 2432.000, 3264.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"cave2tomicro1"
			addSpriteToLayer(null, Block, Layer1Group , 2144.000, 672.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"block"
			addSpriteToLayer(null, Block, Layer1Group , 2144.000, 704.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"block"
			addSpriteToLayer(null, Block, Layer1Group , 2144.000, 736.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"block"
			addSpriteToLayer(null, Block, Layer1Group , 2144.000, 768.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"block"
			addSpriteToLayer(null, Block, Layer1Group , 2176.000, 768.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"block"
			addSpriteToLayer(null, Block, Layer1Group , 2176.000, 736.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"block"
			addSpriteToLayer(null, Block, Layer1Group , 2176.000, 704.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"block"
			addSpriteToLayer(null, Block, Layer1Group , 2176.000, 672.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"block"
			addSpriteToLayer(null, Block, Layer1Group , 2112.000, 672.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"block"
			addSpriteToLayer(null, Block, Layer1Group , 2112.000, 704.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"block"
			addSpriteToLayer(null, Block, Layer1Group , 2112.000, 736.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"block"
			addSpriteToLayer(null, Block, Layer1Group , 2112.000, 768.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"block"
			addSpriteToLayer(null, EnemyBee, Layer1Group , 2912.000, 576.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"bee"
			addSpriteToLayer(null, EnemyBee, Layer1Group , 3136.000, 736.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"bee"
			addSpriteToLayer(null, EnemyBee, Layer1Group , 2976.000, 960.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"bee"
			addSpriteToLayer(null, EnemyBee, Layer1Group , 3296.000, 608.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"bee"
			addSpriteToLayer(new BigPortal(3744.000, 768.000, 770, 1158, "jungle2"), BigPortal, Layer1Group , 3744.000, 768.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"cave2tojungle2"
			addSpriteToLayer(null, Spawner, Layer1Group , 3776.000, 2432.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spawner"
		}

		public function generateObjectLinks(onAddCallback:Function = null):void
		{
		}

	}
}
