package  {
	import org.flixel.*;
	import org.flixel.plugin.photonstorm.*;
	
	public class EnemyTurret extends Enemy {
		[Embed(source='../img/enemyship_2.png')]
		private var ImgShip:Class;
		[Embed(source='../img/enemyshot_1.png')]
		private var ImgBullet:Class;
		[Embed(source='../sfx/enemyshot_1.mp3')]
		private var SfxShot:Class;
		
		public var shotTimer:int = 0;
		
		public function EnemyTurret(X:Number=0, Y:Number=0) {
			super(X, Y, ImgShip);
		}
		
		override public function maxHealth():Number {
			return 15;
		}
		
		override public function damage():Number {
			return 10;
		}
		
		override public function update():void {
			super.update();

			// Shoot.
			if (shotTimer == 0) {
			var dist:FlxPoint = new FlxPoint((PlayState.State().playerShip.x + PlayState.State().playerShip.width / 2) - (x + width / 2),
											 (PlayState.State().playerShip.y + PlayState.State().playerShip.height / 2) - (y + height / 2));
			var distance:Number = Math.sqrt(dist.x * dist.x + dist.y * dist.y);
				
				if (distance < 500) {
					var bullet:EnemyBullet =
						PlayState.State().bullets.recycle(EnemyBullet) as EnemyBullet;
					bullet.loadGraphic(ImgBullet);
					bullet.reset(x + width / 2 - bullet.width / 2, y + height / 2 - bullet.height / 2);
					bullet.damage = 10;
					shotTimer = 120;
					dist.x /= distance;
					dist.y /= distance;
					dist.x *= 5;
					dist.y *= 5;
					bullet.velocity.x = dist.x;
					bullet.velocity.y = dist.y;
					FlxG.play(SfxShot);
				}				
			} else {
				shotTimer--;
			}
		}
	}
}