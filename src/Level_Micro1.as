//Code generated with DAME. http://www.dambots.com

package 
{
	import org.flixel.*;
	import flash.utils.Dictionary;
	public class Level_Micro1 extends BaseLevel
	{
		//Embedded media...
		[Embed(source="../map/mapCSV_Micro1_Map2.csv", mimeType="application/octet-stream")] public var CSV_Map2:Class;
		[Embed(source="../map/tiles_organic.png")] public var Img_Map2:Class;

		//Tilemaps
		public var layerMap2:FlxTilemap;

		//Sprites
		public var Layer1Group:FlxGroup = new FlxGroup;

		//Properties


		public function Level_Micro1(addToStage:Boolean = true, onAddCallback:Function = null, parentObject:Object = null)
		{
			// Generate maps.
			var properties:Array = [];
			var tileProperties:Dictionary = new Dictionary;

			properties = generateProperties( null );
			layerMap2 = addTilemap( CSV_Map2, Img_Map2, 0.000, 0.000, 32, 32, 1.000, 1.000, false, 1, 1, properties, onAddCallback );

			//Add layers to the master group in correct order.
			masterLayer.add(Layer1Group);
			masterLayer.add(layerMap2);

			if ( addToStage )
				createObjects(onAddCallback, parentObject);

			boundsMinX = 0;
			boundsMinY = 0;
			boundsMaxX = 6400;
			boundsMaxY = 3200;
			bgColor = 0xff777777;
		}

		override public function createObjects(onAddCallback:Function = null, parentObject:Object = null):void
		{
			addSpritesForLayerLayer1(onAddCallback);
			generateObjectLinks(onAddCallback);
			if ( parentObject != null )
				parentObject.add(masterLayer);
			else
				FlxG.state.add(masterLayer);
		}

		public function addSpritesForLayerLayer1(onAddCallback:Function = null):void
		{
			addSpriteToLayer(null, EnemyBio, Layer1Group , 3520.000, 2048.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"Bio"
			addSpriteToLayer(null, EnemyBioRam, Layer1Group , 3264.000, 2368.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"bioram"
			addSpriteToLayer(null, EnemyBio, Layer1Group , 3584.000, 2496.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"Bio"
			addSpriteToLayer(null, EnemyBioRam, Layer1Group , 3680.000, 1920.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"bioram"
			addSpriteToLayer(null, EnemyBio, Layer1Group , 3488.000, 2240.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"Bio"
			addSpriteToLayer(null, EnemyBio, Layer1Group , 1216.000, 2272.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"Bio"
			addSpriteToLayer(null, EnemyBioRam, Layer1Group , 1792.000, 2272.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"bioram"
			addSpriteToLayer(null, EnemyBio, Layer1Group , 2208.000, 2080.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"Bio"
			addSpriteToLayer(null, Block, Layer1Group , 2560.000, 2432.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"block"
			addSpriteToLayer(null, Block, Layer1Group , 2560.000, 2464.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"block"
			addSpriteToLayer(null, Block, Layer1Group , 2560.000, 2496.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"block"
			addSpriteToLayer(null, Block, Layer1Group , 2560.000, 2496.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"block"
			addSpriteToLayer(null, Block, Layer1Group , 2592.000, 2496.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"block"
			addSpriteToLayer(null, Block, Layer1Group , 2592.000, 2528.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"block"
			addSpriteToLayer(null, Block, Layer1Group , 2528.000, 2528.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"block"
			addSpriteToLayer(null, Block, Layer1Group , 2560.000, 2528.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"block"
			addSpriteToLayer(null, Block, Layer1Group , 2528.000, 2496.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"block"
			addSpriteToLayer(null, Block, Layer1Group , 2528.000, 2464.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"block"
			addSpriteToLayer(null, Block, Layer1Group , 2592.000, 2432.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"block"
			addSpriteToLayer(null, Block, Layer1Group , 2592.000, 2464.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"block"
			addSpriteToLayer(null, Block, Layer1Group , 2528.000, 2432.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"block"
			addSpriteToLayer(null, Block, Layer1Group , 4000.000, 1376.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"block"
			addSpriteToLayer(null, Block, Layer1Group , 4000.000, 1408.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"block"
			addSpriteToLayer(null, Block, Layer1Group , 4000.000, 1408.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"block"
			addSpriteToLayer(null, Block, Layer1Group , 4032.000, 1408.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"block"
			addSpriteToLayer(null, Block, Layer1Group , 4032.000, 1440.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"block"
			addSpriteToLayer(null, Block, Layer1Group , 4000.000, 1376.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"block"
			addSpriteToLayer(null, Block, Layer1Group , 4032.000, 1376.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"block"
			addSpriteToLayer(null, Block, Layer1Group , 4000.000, 1440.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"block"
			addSpriteToLayer(null, Block, Layer1Group , 4064.000, 1440.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"block"
			addSpriteToLayer(null, Block, Layer1Group , 4064.000, 1408.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"block"
			addSpriteToLayer(null, Block, Layer1Group , 4064.000, 1376.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"block"
			addSpriteToLayer(null, Spawner, Layer1Group , 2624.000, 1088.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spawner"
			addSpriteToLayer(null, LaserRight, Layer1Group , 2368.000, 1024.000, 0.000, 1, 1, false, 1.000, 8.000, generateProperties( null ), onAddCallback );//"laserrigh"
			addSpriteToLayer(null, Boss3Trigger, Layer1Group , 2912.000, 832.000, 0.000, 1, 1, false, 5.000, 20.000, generateProperties( null ), onAddCallback );//"boss3"
			addSpriteToLayer(new BigPortal(480.000, 2336.000, 2541, 3280, "cave2"), BigPortal, Layer1Group , 480.000, 2336.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"micro1tocave2"
			addSpriteToLayer(null, Spawner, Layer1Group , 4288.000, 1408.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spawner"
			addSpriteToLayer(new DialogTrigger(672.000, 2048.000, "Talk about pushing the\nMinimizer Mk II to its limits...\n\nI think I've reached\nthe scale of microbes!\n\n\n\n\n\n\n-Press SPACE to continue-"), DialogTrigger, Layer1Group , 672.000, 2048.000, 0.000, 1, 1, false, 10.000, 20.000, generateProperties( null ), onAddCallback );//"dialogmicro"
		}

		public function generateObjectLinks(onAddCallback:Function = null):void
		{
		}

	}
}
