package  {
	import org.flixel.*;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class IntroState extends FlxState {
		[Embed(source='../mus/Prologue.mp3')]
		private var MusPrologue:Class;
		
		private const text1:String = "Undoubtedly philosophers are in the right,";
		private const text2:String = "when they tell us that nothing is great or little";
		private const text3:String = "otherwise than by comparison.";
		private const text4:String = "--Jonathan Swift, \"Gulliver's Travels\"";
		
		public var t1:FlxText;
		public var t2:FlxText;
		public var t3:FlxText;
		public var t4:FlxText;
		
		public var timer:int;
		
		public function IntroState() {
		}

		override public function create():void {
			super.create();
			
			timer = 0;

			// Text.
			add(t1=new FlxText(0, FlxG.height / 4, FlxG.width, text1).setFormat(null, 16, 0xffffff, 'center', 0x000000));
			add(t2=new FlxText(0, FlxG.height / 4 + 50, FlxG.width, text2).setFormat(null, 16, 0xffffff, 'center', 0x000000));
			add(t3=new FlxText(0, FlxG.height / 4 + 100, FlxG.width, text3).setFormat(null, 16, 0xffffff, 'center', 0x000000));
			add(t4 = new FlxText(350, FlxG.height / 4 + 170, FlxG.width, text4).setFormat(null, 12, 0xffffff, 'left', 0x000000));
			
			t1.alpha = 0;
			t2.alpha = 0;
			t3.alpha = 0;
			t4.alpha = 0;
			
			FlxG.playMusic(MusPrologue);
		}
		
		override public function update():void {
			super.update();
			
			var fade:Number = 0.01;
			
			timer++;
			
			if (timer == 690) {
				FlxG.music.fadeOut(2);
			}
			
			if (timer > 690) {
				t1.alpha -= fade;
				t2.alpha -= fade;
				t3.alpha -= fade;
				t4.alpha -= fade;
			} else {
			if (timer > 60)
				t1.alpha += fade;
			if (timer > 180)
				t2.alpha += fade;
			if (timer > 300)
				t3.alpha += fade;
			if (timer > 460)
				t4.alpha += fade;
			}
				
			if (timer > 830) {
				FlxG.switchState(new PlayState());
			}
		}
	}
}