package  {
    public class Difficulty {
        public static var difficulty:uint = NORMAL;
        
        public static const EASY:uint = 0;
        public static const NORMAL:uint = 1;
        public static const HARD:uint = 2;
		
		public static function maxHealth():Number {
			switch (difficulty) {
				case EASY:
					return 200;
				case NORMAL:
					return 100;
				case HARD:
					return 50;
			}
			return 100;
		}
    }
}