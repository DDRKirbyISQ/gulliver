//Code generated with DAME. http://www.dambots.com

package 
{
	import org.flixel.*;
	import flash.utils.Dictionary;
	public class Level_Space2 extends BaseLevel
	{
		//Embedded media...
		[Embed(source="../map/mapCSV_Space2_Map2.csv", mimeType="application/octet-stream")] public var CSV_Map2:Class;
		[Embed(source="../map/tiles_space.png")] public var Img_Map2:Class;

		//Tilemaps
		public var layerMap2:FlxTilemap;

		//Sprites
		public var Layer1Group:FlxGroup = new FlxGroup;

		//Properties


		public function Level_Space2(addToStage:Boolean = true, onAddCallback:Function = null, parentObject:Object = null)
		{
			// Generate maps.
			var properties:Array = [];
			var tileProperties:Dictionary = new Dictionary;

			properties = generateProperties( null );
			layerMap2 = addTilemap( CSV_Map2, Img_Map2, 0.000, 0.000, 32, 32, 1.000, 1.000, false, 1, 1, properties, onAddCallback );

			//Add layers to the master group in correct order.
			masterLayer.add(Layer1Group);
			masterLayer.add(layerMap2);

			if ( addToStage )
				createObjects(onAddCallback, parentObject);

			boundsMinX = 0;
			boundsMinY = 0;
			boundsMaxX = 8320;
			boundsMaxY = 6400;
			bgColor = 0xff777777;
		}

		override public function createObjects(onAddCallback:Function = null, parentObject:Object = null):void
		{
			addSpritesForLayerLayer1(onAddCallback);
			generateObjectLinks(onAddCallback);
			if ( parentObject != null )
				parentObject.add(masterLayer);
			else
				FlxG.state.add(masterLayer);
		}

		public function addSpritesForLayerLayer1(onAddCallback:Function = null):void
		{
			addSpriteToLayer(new LaserRight(7008.000, 3232.000, true), LaserRight, Layer1Group , 7008.000, 3232.000, 0.000, 1, 1, false, 1.000, 18.000, generateProperties( null ), onAddCallback );//"laserrightactive"
			addSpriteToLayer(null, Spawner, Layer1Group , 5472.000, 3488.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spawner"
			addSpriteToLayer(null, Spawner, Layer1Group , 3680.000, 2144.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spawner"
			addSpriteToLayer(null, EnemyShip, Layer1Group , 4380.000, 2300.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spaceenemy1"
			addSpriteToLayer(null, EnemyShip, Layer1Group , 4316.000, 2492.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spaceenemy1"
			addSpriteToLayer(null, EnemyShip, Layer1Group , 4764.000, 2300.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spaceenemy1"
			addSpriteToLayer(null, EnemyTurret, Layer1Group , 4224.000, 2272.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spaceenemy2"
			addSpriteToLayer(null, EnemyTurret, Layer1Group , 4224.000, 2400.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spaceenemy2"
			addSpriteToLayer(null, EnemyTurret, Layer1Group , 4224.000, 2528.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spaceenemy2"
			addSpriteToLayer(null, EnemyTurret, Layer1Group , 5024.000, 2528.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spaceenemy2"
			addSpriteToLayer(null, EnemyTurret, Layer1Group , 5024.000, 2272.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spaceenemy2"
			addSpriteToLayer(null, EnemyTurret, Layer1Group , 5120.000, 2400.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spaceenemy2"
			addSpriteToLayer(null, EnemyTurret, Layer1Group , 4672.000, 2400.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spaceenemy2"
			addSpriteToLayer(null, EnemyShip, Layer1Group , 4924.000, 2428.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spaceenemy1"
			addSpriteToLayer(null, EnemyShip, Layer1Group , 3164.000, 1756.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spaceenemy1"
			addSpriteToLayer(null, EnemyShip, Layer1Group , 3388.000, 1980.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spaceenemy1"
			addSpriteToLayer(null, EnemyTurret, Layer1Group , 2560.000, 1792.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spaceenemy2"
			addSpriteToLayer(null, EnemyTurret, Layer1Group , 2752.000, 1984.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spaceenemy2"
			addSpriteToLayer(null, EnemyTurret, Layer1Group , 3360.000, 1760.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spaceenemy2"
			addSpriteToLayer(null, EnemyShip, Layer1Group , 2940.000, 1756.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spaceenemy1"
			addSpriteToLayer(null, EnemyTurret, Layer1Group , 3200.000, 1984.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"spaceenemy2"
			addSpriteToLayer(new Portal(736.000, 1088.000, 1311, 1146, "jungle2"), Portal, Layer1Group , 736.000, 1088.000, 0.000, 1, 1, false, 1.000, 1.000, generateProperties( null ), onAddCallback );//"space2tojungle2"
			addSpriteToLayer(null, LaserRight, Layer1Group , 5344.000, 3232.000, 0.000, 1, 1, false, 1.000, 18.000, generateProperties( null ), onAddCallback );//"laserrigh"
			addSpriteToLayer(new DialogTrigger(5568.000, 3200.000, "These sensor readings...\n\nThe station's core must be up ahead!\n\nOnce I destroy it...\n\n\n\n\n\n\n-Press SPACE to continue-"), DialogTrigger, Layer1Group , 5568.000, 3200.000, 0.000, 1, 1, false, 5.000, 20.000, generateProperties( null ), onAddCallback );//"dialogfinalboss"
			addSpriteToLayer(null, Boss1Trigger, Layer1Group , 5888.000, 3136.000, 0.000, 1, 1, false, 5.000, 22.000, generateProperties( null ), onAddCallback );//"boss1"
		}

		public function generateObjectLinks(onAddCallback:Function = null):void
		{
		}

	}
}
