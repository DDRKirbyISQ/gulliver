package {
	import flash.utils.Dictionary;
	import org.flixel.*;
	import org.flixel.plugin.photonstorm.*;
	import org.flixel.plugin.photonstorm.API.FlxKongregate;
	import org.flixel.plugin.photonstorm.FX.StarfieldFX;
	import com.eclecticdesignstudio.motion.Actuate;
	import com.eclecticdesignstudio.motion.easing.Linear;
	
	public class PlayState extends FlxState {
		[Embed(source='../img/ui.png')]
		private var ImgUi:Class;
		[Embed(source='../img/background_space.png')]
		private var ImgSpace:Class;
		[Embed(source='../img/background_organic.png')]
		private var ImgMicro:Class;
		[Embed(source='../img/background_jungle.png')]
		private var ImgJungle:Class;
		[Embed(source='../img/background_cave.png')]
		private var ImgCave:Class;
		[Embed(source='../img/dialogback.png')]
		private var ImgDialog:Class;
		[Embed(source='../sfx/dialogclose.mp3')]
		private var SfxDialogClose:Class;
		[Embed(source='../sfx/dialogopen.mp3')]
		private var SfxDialogOpen:Class;
		
		[Embed(source='../mus/Station Zero.mp3')]
		private var MusSpace:Class;
		[Embed(source='../mus/Thicket.mp3')]
		private var MusJungle:Class;
		[Embed(source='../mus/Cavernous.mp3')]
		private var MusCave:Class;
		[Embed(source='../mus/Flesh.mp3')]
		private var MusMicro:Class;

		public var playerShip:PlayerShip;
		public var currentLevel:BaseLevel;
		public var bullets:FlxGroup;
		public var backdrop:FlxSprite;
		public var starfield:StarfieldFX = FlxSpecialFX.starfield();
		public var stars:FlxSprite;
		public var healthBar:FlxBar;
		public var explosions:ExplosionManager;
		public var dialogText:String;
		public var overlay:FlxSprite;
		public var dialogBack:FlxSprite;
		public var dialogTextObject:FlxText;
		public var light:FlxSprite;
		public var upgrades:FlxGroup;
		public var levelGroup:FlxGroup;
		public var uiCamera:FlxCamera;
		public var ui:FlxSprite;
		public var errorText:FlxText;
		public var saveText:FlxText;
		public var fader:FlxSprite;
		
		public var seenDialogs:Dictionary;
		
		public var level:FlxTilemap;
		
		public var paused:Boolean;
		
		public var respawnX:Number;
		public var respawnY:Number;
		
		public function pause():void {
			paused = true;
		}
		public function unpause():void {
			paused = false;
		}
		
		// Returns this state, assuming that it's the current state.
		public static function State():PlayState {
			return FlxG.state as PlayState;
		}
		
		public function PlayState():void {
		}
		
		public function wedied():void {
			// Fade transition.
			Actuate.tween(fader, 0.5, { alpha: 1 } ).delay(1).onComplete(finishdie).ease(Linear.easeNone);
		}
		
		public function finishdie():void {
			// Fade
			Actuate.tween(fader, 0.5, { alpha: 0 } );
			
			// Respawn
			playerShip.respawn(respawnX, respawnY);
		}
		
		override public function create():void {
			super.create();
			
			//FlxG.visualDebug = true;
			
			// Seen dialogs.
			seenDialogs = new Dictionary();
			
			// Backdrop.
			backdrop = new FlxSprite();
			add(backdrop);
			backdrop.scrollFactor.x = 0;
			backdrop.scrollFactor.y = 0;
			
			// Starfield.
			if (FlxG.getPlugin(FlxSpecialFX) == null) {
				FlxG.addPlugin(new FlxSpecialFX);
			}
			stars = starfield.create(0, 0, FlxG.width, FlxG.height, 30);
			starfield.setStarSpeed( -0.5, 0);
			starfield.setStarDepthColors(5, 0xff666699, 0xffddddff);
			starfield.setBackgroundColor(0x00);
			add(stars);
			stars.scrollFactor.x = 0;
			stars.scrollFactor.y = 0;
			
			// Explosions.
			explosions = new ExplosionManager();
			add(explosions);
			
			// Upgrades.
			upgrades = new FlxGroup();
			add(upgrades);
			
			// Bullets.
			bullets = new FlxGroup();
			add(bullets);
			
			// Level.
			levelGroup = new FlxGroup();
			add(levelGroup);
			
			// Darkness.
			/*light = new FlxSprite(0, 0, ImgLight);
			light.scrollFactor = new FlxPoint(0, 0);
			light.blend = "multiply";
			add(light);*/
			
			// Player ship.
			playerShip = new PlayerShip(900, 850);
			respawnX = playerShip.x;
			respawnY = playerShip.y;
			add(playerShip);
			
			// Fader.
			fader = new FlxSprite();
			fader.makeGraphic(FlxG.width, FlxG.height, 0xff000000);
			fader.scrollFactor = new FlxPoint();
			fader.alpha = 1;
			add(fader);
			
			// UI.
			uiCamera = new FlxCamera(0, 0, FlxG.width, 100);
			FlxG.addCamera(uiCamera);
			ui = new FlxSprite(0, 0, ImgUi);
			add(ui);
			ui.cameras = new Array(uiCamera);
			
			// Health bar.
			healthBar = new FlxBar(90, 30, FlxBar.FILL_LEFT_TO_RIGHT, FlxG.width - 120, 10, playerShip, "health", 0, Difficulty.maxHealth(), true);
			add(healthBar);
			healthBar.scrollFactor.x = 0;
			healthBar.scrollFactor.y = 0;
			var healthText:FlxText;
			healthText = new FlxText(10, 23, FlxG.width, 'Shields').setFormat(null, 16);
			healthText.scrollFactor.x = 0;
			healthText.scrollFactor.y = 0;
			add(healthText);
			healthBar.cameras = new Array(uiCamera);
			healthText.cameras = new Array(uiCamera);
			
			// Error text.
			errorText = new FlxText(0, 70, FlxG.width, "Can't unshrink near obstacles").setFormat(null, 16, 0xff0000, 'center', 0x000000);
			errorText.cameras = new Array(uiCamera);
			errorText.alpha = 0;
			add(errorText);
			
			// Save text.
			saveText = new FlxText(0, 70, FlxG.width, "Spawn point set").setFormat(null, 16, 0x00ff00, 'center', 0x000000);
			saveText.cameras = new Array(uiCamera);
			saveText.alpha = 0;
			add(saveText);
			
			// Dialog.
			overlay = new FlxSprite();
			overlay.makeGraphic(FlxG.width, FlxG.height, 0x55000000);
			overlay.visible = false;
			overlay.scrollFactor.x = 0;
			overlay.scrollFactor.y = 0;
			add(overlay);
			dialogBack = new FlxSprite(0, 0, ImgDialog);
			dialogBack.visible = false;
			dialogBack.scrollFactor = new FlxPoint();
			FlxDisplay.screenCenter(dialogBack, true, true);
			dialogBack.y -= 40;
			add(dialogBack);
			dialogText = "";
			dialogTextObject = new FlxText(0, FlxG.height / 2 - 120 - 40, FlxG.width, "").setFormat(null, 16, 0xffffff, "center", 0x000000);
			dialogTextObject.scrollFactor.x = 0;
			dialogTextObject.scrollFactor.y = 0;
			add(dialogTextObject);
			
			// Set up camera.
			FlxG.camera.height = FlxG.height - 100;
			FlxG.camera.y = 100;
			FlxG.camera.follow(playerShip);
			
			// Load level.
			currentLevel = new Level_Space1(true, null, levelGroup);
			switchLevel(new Level_Space1(true, null, levelGroup), false, false);
			
			//currentLevel.masterLayer.members[0].add(new Boss1(500, 1000));
			
			//playerShip.x = 5279;
			//playerShip.y = 3201;
			//switchLevel(new Level_Space2(true, null, levelGroup), false, false);
			//playerShip.weaponType = PlayerShip.WEAPON_GREEN;
			//2340, 2854
			
			
            Actuate.tween(fader, 5, { alpha: 0 } );
		}
		
		override public function update():void {
			errorText.alpha -= 0.05;
			saveText.alpha -= 0.01;
			
			if (errorText.alpha <= 0) {
				errorText.exists = false;
			} else {
				errorText.exists = true;
			}
			if (saveText.alpha <= 0) {
				saveText.exists = false;
			} else {
				saveText.exists = true;
			}
			if (fader.alpha == 0) {
				fader.exists = false;
			} else {
				fader.exists = true;
			}
			
			if (paused) {
				// dont do anything
			} else if (dialogText != "") {
				// Don't update anything, we're paused.
				if (FlxG.keys.justPressed("SPACE")) {
					dialogText = "";
					dialogTextObject.text = "";
					overlay.visible = false;
					dialogBack.visible = false;
					FlxG.play(SfxDialogClose);
				}
			} else {
				super.update();
			}
		}
		
		public function switchLevel(level:BaseLevel, zoom:Boolean, goingSmall:Boolean):void {
			
			
			currentLevel.masterLayer.kill();
			currentLevel = level;
			bullets.clear();
			upgrades.clear();
			explosions.clear();
			FlxG.camera.setBounds(BaseLevel.boundsMinX, BaseLevel.boundsMinY,
				BaseLevel.boundsMaxX - BaseLevel.boundsMinX,
				BaseLevel.boundsMaxY - BaseLevel.boundsMinY, true);
				
			stars.exists = false;
			if (level is Level_Space1) {
				backdrop.loadGraphic(ImgSpace);
				stars.exists = true;
				FlxG.playMusic(MusSpace);
			} else if (level is Level_Space2) {
				backdrop.loadGraphic(ImgSpace);
				stars.exists = true;
				FlxG.playMusic(MusSpace);
			} else if (level is Level_Jungle1) {
				backdrop.loadGraphic(ImgJungle);
				FlxG.playMusic(MusJungle);
			} else if (level is Level_Jungle2) {
				backdrop.loadGraphic(ImgJungle);
				FlxG.playMusic(MusJungle);
			} else if (level is Level_Cave1) {
				backdrop.loadGraphic(ImgCave);
				FlxG.playMusic(MusCave);
			} else if (level is Level_Cave2) {
				backdrop.loadGraphic(ImgCave);
				FlxG.playMusic(MusCave);
			} else if (level is Level_Micro1) {
				backdrop.loadGraphic(ImgMicro);
				FlxG.playMusic(MusMicro);
			} else {
				// Other levels...
			}
			
			if (zoom) {
				if (goingSmall) {
					if (playerShip.shrunk) {
						playerShip.shrinkInstant();
					}
					FlxG.camera.zoom = 1;
					// Zooming out DOESNT WORK, so don't try it.
					// Start zoomed out, then zoom in
					//FlxG.camera.zoom = 0.25;
					//Actuate.tween(FlxG.camera, 2, { zoom: 1 }).ease(Linear.easeNone);
				} else {
					if (!playerShip.shrunk) {
						playerShip.shrinkInstant();
					}
					// Start zoomed in, then zoom out
					// This looks kind of suspect too
					//FlxG.camera.zoom = 4;
					//Actuate.tween(FlxG.camera, 2, { zoom: 1 }).ease(Linear.easeNone);
				}
			} else {
				FlxG.camera.zoom = 1;
				Actuate.reset();
			}
		}
		
		public function dialog(text:String):void {
			dialogText = text;
			overlay.visible = true;
			dialogBack.visible = true;
			dialogTextObject.text = text;
			FlxG.play(SfxDialogOpen);
		}

		override public function destroy():void {
			super.destroy();
		}
	}
}