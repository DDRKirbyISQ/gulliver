package  {
	import org.flixel.*;
	import org.flixel.plugin.photonstorm.*;
	
	public class Boss3Trigger extends FlxSprite {
		[Embed(source='../img/dialog.png')]
		private var ImgRect:Class;
		[Embed(source='../mus/Destiny.mp3')]
		private var MusBoss:Class;
		
		public var triggerText:String;
		
		public function Boss3Trigger(X:Number=0, Y:Number=0) {
			super(X, Y, ImgRect);
			triggerText = "boss3";
			visible = false;
			//TODO: disable debug display!
			
			if (PlayState.State().seenDialogs[triggerText] == "yes") {
				exists = false;
			}
		}
		
		override public function update():void {
			super.update();
			
			if (FlxG.overlap(this, PlayState.State().playerShip)) {
				
				// spawn boss 2
				PlayState.State().currentLevel.masterLayer.members[0].add(new Boss3(3516, 1088));
				
				// switch music
				FlxG.playMusic(MusBoss);
				
				exists = false;
				
				// Mark as seen.
				PlayState.State().seenDialogs[triggerText] = "yes";
			}
		}
	}
}