package  {
	import org.flixel.*;
	
	public class ExplosionManager extends FlxGroup {
		public function Explode(x:int, y:int, number:int):void {
			var emitter:FlxEmitter = recycle(FlxEmitter) as FlxEmitter;
			
			emitter.x = x;
			emitter.y = y;
			emitter.maxRotation = 0;
			emitter.setRotation(0, 0);
			emitter.maxParticleSpeed = new FlxPoint(200, 200);
			emitter.minParticleSpeed = new FlxPoint( -200, -200);
			for (var i:int = 0; i < number; ++i) {
				var particle:FlxParticle = new FlxParticle();
				particle.makeGraphic(2, 2);
				particle.exists = false;
				particle.antialiasing = false;
				emitter.add(particle);
			}
			emitter.start(true, 1);
			add(emitter);
		}
	}
}